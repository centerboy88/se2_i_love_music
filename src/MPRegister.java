import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import base.MusicPlattform;
import base.Database;

public class MPRegister {
	
	private MusicPlattform plattform;
	private static final Logger LOG = Logger.getLogger(MPRegister.class.getName());


	private JFrame frmRegistrierung;
	private JTextField txtUsername;
	private JPasswordField pwdPassword;
	private JPasswordField pwdPasswordcheck;
	private JTextField txtEmail;
	private Database db = new Database();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPRegister window = new MPRegister(new MusicPlattform());
					window.frmRegistrierung.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void show(final MusicPlattform mp) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPRegister window = new MPRegister(mp);
					window.frmRegistrierung.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MPRegister(MusicPlattform mp) {
		this.plattform = mp;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRegistrierung = new JFrame();
		frmRegistrierung.setTitle("Registrierung");
		frmRegistrierung.getContentPane().setLayout(null);
		frmRegistrierung.setBounds(150, 150, 455, 319);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(65, 27, 134, 40);
		frmRegistrierung.getContentPane().add(txtUsername);
		txtUsername.setColumns(10);
		
		pwdPassword = new JPasswordField();
		pwdPassword.setBounds(185, 79, 237, 40);
		frmRegistrierung.getContentPane().add(pwdPassword);
		
		pwdPasswordcheck = new JPasswordField();
		pwdPasswordcheck.setBounds(185, 142, 237, 40);
		frmRegistrierung.getContentPane().add(pwdPasswordcheck);
		
		JButton btnRegistrieren = new JButton("Registrieren");
		btnRegistrieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = txtUsername.getText();
				String pass = new String(pwdPassword.getPassword());
				String passCheck = new String(pwdPasswordcheck.getPassword());
				String email = txtEmail.getText();
				if(username.equals("") || pass.equals("") || passCheck.equals("") || email.equals("")){
					JOptionPane.showMessageDialog(frmRegistrierung, "Bitte alle Felder ausfuellen", "Eingabe unvollstaendig", JOptionPane.ERROR_MESSAGE);
				} else {
					if(db.userNameExist(username)){
						JOptionPane.showMessageDialog(frmRegistrierung, "Benutzername ist bereits vergeben", "Nicht verfuegbar", JOptionPane.ERROR_MESSAGE);
					} else {
						if(plattform.validateEmailAdress(email)){
							LOG.info("Registrieren");
							LOG.info("Passwort:" + pass + ", Check:" + passCheck);
							if(pass.equals(passCheck)) {
								LOG.fine("Passwort ok.");
								plattform.register(username, pass, email);
								LOG.fine("Neues Profil erzeugt.");
								JOptionPane.showMessageDialog(frmRegistrierung, "Registrierung erfolgreich", "Erfolg", JOptionPane.INFORMATION_MESSAGE);
								frmRegistrierung.dispose();
							} else {
								LOG.log(Level.WARNING, "Passwörter stimmen nicht überein.");
								JOptionPane.showMessageDialog(frmRegistrierung, "Ihre Passwoerter stimmen nicht ueberein", "Eingabefehler", JOptionPane.ERROR_MESSAGE);

							 }
						} else {
							JOptionPane.showMessageDialog(frmRegistrierung, "Keine gueltige Emailadresse", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		btnRegistrieren.setBounds(65, 235, 134, 25);
		frmRegistrierung.getContentPane().add(btnRegistrieren);
		
		JButton btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmRegistrierung.dispose();
			}
		});
		btnAbbrechen.setBounds(237, 235, 134, 25);
		frmRegistrierung.getContentPane().add(btnAbbrechen);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(277, 27, 145, 40);
		frmRegistrierung.getContentPane().add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(12, 27, 52, 25);
		frmRegistrierung.getContentPane().add(lblName);
		
		JLabel lblPasswort = new JLabel("Passwort");
		lblPasswort.setBounds(23, 86, 93, 25);
		frmRegistrierung.getContentPane().add(lblPasswort);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(217, 27, 52, 25);
		frmRegistrierung.getContentPane().add(lblEmail);
		
		JLabel lblPasswortcheck = new JLabel("<HTML><BODY>Passwort erneut<BR>eingeben</BODY></HTML>");
		lblPasswortcheck.setBounds(23, 149, 134, 40);
		frmRegistrierung.getContentPane().add(lblPasswortcheck);
	}
}
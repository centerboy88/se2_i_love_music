
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.Timer;


public class MPGutscheinverwaltung {

	private JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPGutscheinverwaltung window = new MPGutscheinverwaltung();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 *  show funktion zeigt das fenster an
	 */
	public void show(){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPGutscheinverwaltung window = new MPGutscheinverwaltung();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MPGutscheinverwaltung() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		Timer t = new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
        		table.setModel(new MPTableModelCoupon());
            }
        });
        t.start();
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 550, 400);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Gutscheinverwaltung");
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(23, 13, 524, 293);
		frame.getContentPane().add(scrollPane);
		frame.setTitle("Gutscheinverwaltung");
		
		/*
		 * Tabelle Coupons erstellen
		 * 
		 */
		
		
		table = new JTable(new MPTableModelCoupon());
		scrollPane.setViewportView(table);
		
		/*
		 * Gutscheine erstellen
		 */
		JButton btnNeuenGutscheinErstellen = new JButton("Neuen Gutschein erstellen");
		btnNeuenGutscheinErstellen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MPGutscheinErstellen frameGutscheinErstellen = new MPGutscheinErstellen();
				frameGutscheinErstellen.show();
			}
		});
		btnNeuenGutscheinErstellen.setBounds(90, 317, 224, 25);
		frame.getContentPane().add(btnNeuenGutscheinErstellen);
		
		
		JButton btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		btnAbbrechen.setBounds(326, 317, 117, 25);
		frame.getContentPane().add(btnAbbrechen);		
	}
}

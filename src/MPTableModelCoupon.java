import javax.swing.table.AbstractTableModel;

import base.Coupon;


@SuppressWarnings("serial")
class MPTableModelCoupon extends AbstractTableModel {
    private String[] columnNames = new String[] {"Code", "Wert", "imUmlauf"};
    private Object[][] data;
    
    public MPTableModelCoupon() {
    	super();
    	data = Coupon.conv(Coupon.getAll());
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.length;
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
        return data[row][col];
    }

    public Class<?> getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    /*
     * Don't need to implement this method unless your table's
     * editable.
     */
    public boolean isCellEditable(int row, int col) {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
    	if(col == 6) {
    		return true;
    	} else {
    		return false;
    	}
    }

    /*
     * Don't need to implement this method unless your table's
     * data can change.
     */
    public void setValueAt(Object value, int row, int col) {
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }

}
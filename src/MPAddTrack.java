
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import base.Database;
import base.MuFi;
import base.MusicPlattform;


public class MPAddTrack {
	
	private MusicPlattform plattform;

	private JFrame frame;
	private JTextField txtInterpret;
	private JTextField txtTitel;
	private JLabel lblTitel;
	private JLabel lblAlbum;
	private JTextField txtAlbum;
	private JLabel lblGenre;
	private JTextField txtGenre;
	private JLabel lblPreis;
	private JTextField txtPreis;
	private Database db = new Database();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPAddTrack window = new MPAddTrack(new MusicPlattform());
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void show(final MusicPlattform mp) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPAddTrack window = new MPAddTrack(mp);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MPAddTrack(MusicPlattform mp) {
		this.plattform = mp;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 300, 250);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Titel hinzufuegen");
		
		txtInterpret = new JTextField();
		txtInterpret.setBounds(88, 10, 114, 19);
		frame.getContentPane().add(txtInterpret);
		txtInterpret.setColumns(10);
		
		JLabel lblInterpret = new JLabel("Interpret:");
		lblInterpret.setBounds(12, 12, 70, 15);
		frame.getContentPane().add(lblInterpret);
		
		txtTitel = new JTextField();
		txtTitel.setBounds(88, 37, 114, 19);
		frame.getContentPane().add(txtTitel);
		txtTitel.setColumns(10);
		
		lblTitel = new JLabel("Titel:");
		lblTitel.setBounds(12, 39, 70, 15);
		frame.getContentPane().add(lblTitel);
		
		lblAlbum = new JLabel("Album:");
		lblAlbum.setBounds(12, 66, 70, 15);
		frame.getContentPane().add(lblAlbum);
		
		txtAlbum = new JTextField();
		txtAlbum.setBounds(88, 64, 114, 19);
		frame.getContentPane().add(txtAlbum);
		txtAlbum.setColumns(10);
		
		lblGenre = new JLabel("Genre:");
		lblGenre.setBounds(12, 93, 70, 15);
		frame.getContentPane().add(lblGenre);
		
		txtGenre = new JTextField();
		txtGenre.setBounds(88, 91, 114, 19);
		frame.getContentPane().add(txtGenre);
		txtGenre.setColumns(10);
		
		lblPreis = new JLabel("Preis:");
		lblPreis.setBounds(12, 123, 70, 15);
		frame.getContentPane().add(lblPreis);
		
		txtPreis = new JTextField();
		txtPreis.setBounds(88, 121, 114, 19);
		frame.getContentPane().add(txtPreis);
		txtPreis.setColumns(10);
		
		JButton btnHinzufuegen = new JButton("Hinzufuegen");
		btnHinzufuegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// hinzufuegen
				String interpret = txtInterpret.getText();
				String title =  txtTitel.getText();
				String album =  txtAlbum.getText();
				String genre =  txtGenre.getText();
				// SPEICHERN
				if(interpret.equals("") || genre.equals("") || album.equals("") || 
				   title.equals("") || (txtPreis.getText().equals("") || txtPreis.getText().equals("0") )){
					
					JOptionPane.showMessageDialog(frame,"leere oder ungueltige Eingabe","Titel hinzufuegen", JOptionPane.ERROR_MESSAGE);			
				}
				else{
					String price_old = txtPreis.getText().replace(",", ".");
					float price =  Float.parseFloat(price_old);
					MuFi file =  new MuFi(title,interpret,album,genre, price);
					db.insertMuFi(file);
					txtInterpret.setText("");
					txtTitel.setText("");
					txtAlbum.setText("");
					txtGenre.setText("");
					txtPreis.setText("");
					JOptionPane.showMessageDialog(frame,"neuer Titel erfolgreich erstellt","Tite erstellen", JOptionPane.INFORMATION_MESSAGE);			
					
					
				}
			}
		});
		btnHinzufuegen.setBounds(12, 159, 132, 25);
		frame.getContentPane().add(btnHinzufuegen);
		
		JButton btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnAbbrechen.setBounds(169, 159, 117, 25);
		frame.getContentPane().add(btnAbbrechen);
	}
}

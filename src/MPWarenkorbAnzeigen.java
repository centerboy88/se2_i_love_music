import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import base.MusicPlattform;
import base.Profile;


public class MPWarenkorbAnzeigen {

	private JFrame frmIhrPersoenlicherWarenkorb;
	private JTable table;
	private JScrollPane scrollPane;
	private MusicPlattform plattform;
	
	private String[] columnNames = new String[] {"", "Titel", "Interpret", "Album", "Genre", "Preis", "Loeschen" };
	private int[] columnsEditable = new int[] {6};
	private JLabel lblShowCredits;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPWarenkorbAnzeigen window = new MPWarenkorbAnzeigen(new MusicPlattform());
					window.frmIhrPersoenlicherWarenkorb.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */	
	public MPWarenkorbAnzeigen(MusicPlattform mp) {
		this.plattform = mp;
		initialize();
	}
	
	public void show(final MusicPlattform mp) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPWarenkorbAnzeigen window = new MPWarenkorbAnzeigen(mp);
					window.frmIhrPersoenlicherWarenkorb.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmIhrPersoenlicherWarenkorb = new JFrame();
		frmIhrPersoenlicherWarenkorb.setTitle("Ihr persoenlicher Warenkorb");
		frmIhrPersoenlicherWarenkorb.setBounds(100, 100, 550, 400);
		frmIhrPersoenlicherWarenkorb.getContentPane().setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 40, 524, 237);
		frmIhrPersoenlicherWarenkorb.getContentPane().add(scrollPane);
		
		
		lblShowCredits = new JLabel();
		lblShowCredits.setBounds(157, 0, 249, 28);
		lblShowCredits.setVisible(false);
		frmIhrPersoenlicherWarenkorb.getContentPane().add(lblShowCredits);
		
		if(plattform.getUser() != null) {
			lblShowCredits.setText("Credits: " + plattform.getUser().getUserCredit());
			lblShowCredits.setVisible(true);
		}
		
		table = new JTable(new MPTableModelMusic(plattform, columnNames, columnsEditable));
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(0).setWidth(0);
		scrollPane.setViewportView(table);
		
		JButton btnAuswahlLoeschen = new JButton("Auswahl loeschen");
		btnAuswahlLoeschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < table.getModel().getRowCount(); i++) {
					if ((boolean) table.getModel().getValueAt(i, 6)) {
						plattform.removeFileFromShoppingCart((int) table.getModel().getValueAt(i, 0));
					}
				}				
				table.setModel(new MPTableModelMusic(plattform, columnNames, columnsEditable));
				table.getColumnModel().getColumn(0).setMinWidth(0);
				table.getColumnModel().getColumn(0).setMaxWidth(0);
				table.getColumnModel().getColumn(0).setWidth(0);
			}
		});
		
		
		btnAuswahlLoeschen.setBounds(59, 289, 162, 25);
		frmIhrPersoenlicherWarenkorb.getContentPane().add(btnAuswahlLoeschen);
		
		JButton btnKaufen = new JButton("Kaufen");
		btnKaufen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Profile p = plattform.getUser();
				if(plattform.isCartEmpty()) {
					JOptionPane.showMessageDialog(frmIhrPersoenlicherWarenkorb, "Ihr Warenkorb ist leer", "Fehler", JOptionPane.ERROR_MESSAGE);
				}
				else if (p == null) {
					JOptionPane.showMessageDialog(frmIhrPersoenlicherWarenkorb, "Melden Sie sich zum Kauf des Warenkorbs an", "Nicht angemeldet", JOptionPane.ERROR_MESSAGE);
				}
				else if(plattform.buyShoppingCart(p)) {
					JOptionPane.showMessageDialog(frmIhrPersoenlicherWarenkorb, "Die Titel aus Ihrem Warenkorb wurden Ihrem Profil hinzugefuegt", "Kauf erfolgreich", JOptionPane.INFORMATION_MESSAGE);
					lblShowCredits.setText("Credits: " + plattform.getUser().getUserCredit());
				}
				else {
					JOptionPane.showMessageDialog(frmIhrPersoenlicherWarenkorb, "Sie haben nicht genuegend Credits fuer diesen Kauf", "Kauf fehlgeschlagen", JOptionPane.ERROR_MESSAGE);
				}
				table.setModel(new MPTableModelMusic(plattform, columnNames, columnsEditable));
			}
		});
		
		
		btnKaufen.setBounds(301, 289, 130, 25);
		frmIhrPersoenlicherWarenkorb.getContentPane().add(btnKaufen);
		
		JButton btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmIhrPersoenlicherWarenkorb.dispose();
			}
		});
		btnAbbrechen.setBounds(59, 326, 162, 25);
		frmIhrPersoenlicherWarenkorb.getContentPane().add(btnAbbrechen);
		
		JButton btnAktualisieren = new JButton("Aktualisieren");
		btnAktualisieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				table.setModel(new MPTableModelMusic(plattform, columnNames, columnsEditable));
				table.getColumnModel().getColumn(0).setMinWidth(0);
				table.getColumnModel().getColumn(0).setMaxWidth(0);
				table.getColumnModel().getColumn(0).setWidth(0);
			}
		});
		btnAktualisieren.setBounds(301, 326, 130, 25);
		frmIhrPersoenlicherWarenkorb.getContentPane().add(btnAktualisieren);
	}
}
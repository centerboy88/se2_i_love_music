package base;
import java.util.ArrayList;
import java.util.List;

import base.exceptions.NoProfileFoundException;
import base.exceptions.NotEqualException;
import base.exceptions.NotFoundException;



/**
 * @author Dennis, Rene, Benjamin, Fritz, Michael
 * @version 1.1
 *
 *Description: die Klasse Profile erstellt einen Nutzer und initialisiert ihn, speichert ihn in der Datenbank und ruft die Registration Klasse auf 
 */

public class Profile implements Cloneable, Comparable<Profile>  {
	
	private Credit userCredit;
	/**
	 * ist das Guthaben des Profils 
	 * HasGetter
	 * HasSetter
	 */
	private String userName;
	/**
	 * ist der Name des userŽs dem das Profil zugeordnet wird 
	 * HasGetter
	 * HasSetter
	 */
	private boolean userStatus;
	/**
	 * ist Aktivierungsstatus des users 
	 * HasGetter
	 * HasSetter
	 */
	private String userEmail;
	/**
	 * ist die E-Mai des Users   
	 * HasGetter
	 * HasSetter
	 */
	private int uid;
	/**
	 * ist die eindeutige Nummer die dem User zugeordnet wird   
	 * HasGetter
	 */
	private ArrayList<MuFi> userTitleList;
	/**
	 * ist die Liste der bereits gekauften Tietel des Profils  
	 * HasGetter
	 */
	private ArrayList<MuFi> userPlaylist;
	/**
	 * ist die vom User erstellte Liste von MusicFiles
	 * HasGetter
	 */
	private Password userPassword;
	/**
	 * ist die dem User zugeordnete Rolle
	 * HasGetter
	 * HasSetter
	 */
	private Role userRole;
	private String verificationCode;
	private final static Database DATABASE = new Database();
	
	/**
	 * @return the verificationCode
	 */
	public String getVerificationCode() {
		return verificationCode;
	}

	/**
	 * @param verificationCode the verificationCode to set
	 */
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	/**
	 * Konstruktor erstellt neuen User und speichert ihn ihn der Datenbank, ruft die Aktivierungsmethode auf
	 * 
	 */	
	public Profile(String uname, String email, String pass) {
		this.userName = uname;
		this.userEmail = email;
		this.userCredit = new Credit(0);
		this.userStatus = false;
		this.userRole = Role.user;
		this.uid = DATABASE.createUid();
		this.userTitleList = new ArrayList<MuFi>();
		this.userPlaylist = new ArrayList<MuFi>();
		this.userPassword = new Password(pass);
	}
	
	
	/**
	 * Konstruktoren die fur die Datenbank benoetigt werden  
	 * @param uid ist die eindeutige UserID
	 * @param name ist der Name des Nutzers
	 * @param email	ist die Emailadresse des Nutzers
	 * @param pass ist das gewuenschte Passwort des Nutzers
	 */
	public Profile(int uid, String name, String email, String pass, Credit c, boolean status, Role role,
			ArrayList<MuFi> userTitleList, ArrayList<MuFi> userPlaylist) {
		this.uid = uid;
		this.userName = name;
		this.userEmail = email;
		this.userCredit = c;
		this.userStatus = status;
		this.userRole = role;
		this.userTitleList = userTitleList;
		this.userPlaylist = userPlaylist;
		this.userPassword = new Password(pass);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		final Profile profile = (Profile) super.clone();
		profile.uid = this.uid;
		profile.userName = this.userName;
		profile.userEmail = this.userEmail;
		profile.userPassword = this.userPassword.clone();
		profile.userCredit = this.userCredit.clone();
		profile.userStatus = this.userStatus;
		profile.userRole = this.userRole;
		profile.userTitleList = (ArrayList<MuFi>) this.userTitleList.clone();
		profile.userPlaylist = (ArrayList<MuFi>) this.userPlaylist.clone();
		return profile;
	}
	
	/**
	 * Getter fur das Profil 
	 * @param username ist der User der zurueck gegeben werden soll
	 * @return ist der zurueck gegebene User
	 * @throws NoProfileFoundException wird geworfen wenn das gesuchte Profil nicht existiert
	 */
	public static Profile getProfile(String username) throws NoProfileFoundException{
		Profile p = DATABASE.getProfile(username);
		if(p == null) throw new NoProfileFoundException();
		return p;
	}
	
	//
	// GETTER AND SETTER
	//
	public float getUserCredit() {
		return userCredit.getCredit();
	}

	public String getUserName() {
		return userName;
	}

	public boolean getUserStatus() {
		return userStatus;
	}
	
	public Role getUserRole() {
		return userRole;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public int getUid() {
		return uid;
	}

	public ArrayList<MuFi> getUserTitleList() {
		return this.userTitleList;
		
	}

	public ArrayList<MuFi> getUserPlaylist() {
		return this.userPlaylist;
	}

	public void setUserName(String userName) {
		this.userName = userName;
		updateProfile();
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
		updateProfile();
	}
	public void setUserCredit(float value){
		this.userCredit = new Credit(value);
		updateProfile();
	}
	
	public void setUserStatus(boolean userStatus) {
		this.userStatus = userStatus;
		updateProfile();
	}
	
	public void setUserRole(Role role) {
		this.userRole = role;
		updateProfile();
	}
	
	/**
	 * veraendert das Passwort 
	 * @param pass ist das neue Passwort
	 * @throws NotEqualException wird geworfen wenn das alte Passwort nicht mit dem aktuellen Passwort uebereinstimmen 
	 */
	public void changePassword(String pass) {
		this.userPassword.changePassword(pass);
		updateProfile();
	}
	
	/**
	 * veraendert die User EMail
	 * @param newUserEmail ist die neue Mailadresse das Users 
	 * @param checkUserEmail ist die alte Mail zum Vergleich
	 * @throws NotEqualException  wird geworfen wenn das alte user Passwort nicht mit dem check Passwort uebereinstimmen
	 */
	public void changeUserEmail(String newUserEmail, String checkUserEmail) throws NotEqualException{
		if(newUserEmail.equals(checkUserEmail)){
			this.setUserEmail(newUserEmail);
			updateProfile();
		}
		else{
			throw new NotEqualException("Your entered Email and your check Email are not equal please try again");
		}
	}
	
	/**
	 * prueft das Profil in der Datenbank mit dem aktuelllen und aendert gegebenenfalls die die Werte 
	 * @return wenn es funktioniert hat wird ein true zurueck gegeben 
	 */
	public boolean updateProfile(){
		DATABASE.setProfile(this);
		return true;
	}
	
	/**
	 * ist die checkPasswort Funktion aus Password
	 * @param pass ist das zupruefende Passwort 
	 * @return gibt das Ergebnis der checkPasswort Funktion zurueck 
	 */
	public boolean checkPassword(String pass) {
		return this.userPassword.checkPassword(pass);
	}

	//
	// PlayList Funktionen
	//
	
	/**
	 * gibt die Stelle zurueck an der ein bestimmter Titel in einer Liste steht
	 * @param element ist das zusuchende Element
	 * @param list ist die Liste in der gesucht werden soll
	 * @return die Stelle an der Titel steht
	 */
	public int getIndexOfList(MuFi element , ArrayList<MuFi> list){
		return list.indexOf(element);
	}
	
	/**
	 * fuegt einen Titel an das Ende einer Liste
	 * @param element ist das hinzu zu fuegende Element
	 * @return true falls es Erfolgreich war
	 */
	public boolean addTitelEndToPlayList( MuFi element){
		if(!this.userPlaylist.contains(element)) {
			this.userPlaylist.add(element);
			updateProfile();
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * fuegt einen Titel an eine bestimmte Stelle einer Liste
	 * @param element ist das hinzu zu fuegende Element
	 * @return true falls es Erfolgreich war
	 */	
	public boolean addTitelOnIndexToPlayList( MuFi element, ArrayList<MuFi> arrayList, int index){
		if(!this.userPlaylist.contains(element)) {
			this.userPlaylist.add(index,element);
			updateProfile();
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * loescht einen Titel aus einer Liste 
	 * @param list ist die Liste aus der geloescht werden soll
	 * @param element ist das zu loeschende Element
	 * @return true falls erflogreich 
	 */
	public boolean removeTitleFromPlayList(ArrayList<MuFi> list, MuFi element) throws ArrayIndexOutOfBoundsException {
		final int index = list.indexOf(element);
		list.remove(index);
		this.userPlaylist = list;
		updateProfile();
		return true;
	}
	
	/**
	 * leert die ganze Liste
	 * @return true falls erfolgreich 
	 */
	public boolean clearPlayList(){
		this.userPlaylist.clear();
		updateProfile();
		return true;
	}
	
	
	//
	// TitleList Funktionen
	// 
	/**
	 * fuegt einen Titel an das Ende einer Liste
	 * @param element ist das hinzu zu fuegende Element
	 * @return true falls es Erfolgreich war
	 */
	public boolean addTitelEndToTitleList( MuFi element){
		this.userTitleList.add(element);
		updateProfile();
		return true;
	}
	
	/**
	 * fuegt einen Titel an eine bestimmte Stelle einer Liste
	 * @param element ist das hinzu zu fuegende Element
	 * @return true falls es Erfolgreich war
	 */
	public boolean addTitelOnIndexToTitleList( MuFi element, ArrayList<MuFi> arrayList, int index){
		this.userTitleList.add(index,element);
		updateProfile();
		return true;
	}
	
	/**
	 * loescht einen Titel aus einer Liste 
	 * @param list ist die Liste aus der geloescht werden soll
	 * @param element ist das zu loeschende Element
	 * @return true falls erflogreich 
	 */
	public boolean removeTitleFromTitleList(ArrayList<MuFi> list, MuFi element) throws ArrayIndexOutOfBoundsException {
		final int index = list.indexOf(element);
		list.remove(index);
		this.userTitleList = list;
		updateProfile();
		return true;
	}
	
	/**
	 * Prueft das vorhanden sein, des gewuenschten download Titels in der Benutzer Titelliste und gibt Titel oder Fehlermeldung zurueck
	 * 
	 * @param element Uebergabe des zu downloadenden Titels
	 * @return gibt download path zurueck 
	 * @throws NotFoundException wird geworfen wenn der Titel nicht in der UserTitelList enthalten ist 
	 */
	public String downloadMuFi(final MuFi element) throws NotFoundException{
		if(this.userTitleList.contains(element)){
		return element.getPath();
		}
		else{
			throw new NotFoundException("Die zu Downloadende Datei befindet sich nicht in Ihrer Titelsammlung");
		}
	}
	
	/**
	 * gibt einen Liste mit Downloadpfaden zurueck von den Titeln, welche der User gekauft hat
	 * @return ArrayList<String>
	 */
	public List<String> downloadMusicFileList() {
		final ArrayList<String> resultPathList = new ArrayList<String>(100);
		for(MuFi m1: userTitleList){
			resultPathList.add(m1.getPath());
		}
		return resultPathList;
	}
	
	/**
	 * gibt einen Liste mit Downloadpfaden zurueck von den Titeln, welche der User in seiner Playliste hat
	 * @return ArrayList<String> eine Liste mit den Downloadpfaden
	 */
	public List<String> downloadPlaylist() {
		final ArrayList<String> resultPathList = new ArrayList<String>(100);
		for(MuFi m1: userPlaylist){
			resultPathList.add(m1.getPath());
		}
		return resultPathList;
	}
	
	/**
	 * loescht die TitleList
	 * @return true falls erfolgreich
	 */
	public boolean clearTitleList(){
		userTitleList.clear();
		updateProfile();
		return true;
	}

	@Override
	public int compareTo(Profile profile) {
		return ((Integer) this.uid).compareTo(profile.uid);
	}
	
	@Override
	public String toString() {
		return "\n[" + this.uid + "," + this.userName + "," + this.userPassword + "," + this.getUserCredit() + ", \n\tMusic:{" + 
				this.userTitleList.toString() + "}]";
	}
	
	public String getPassword() {
		return this.userPassword.toString();
	}
	
}

package base;

/**
*	Diese Klasse implementiert Musikdateien
*	und stellt die dazugehoerigen Funktionalitaeten bereit.
*	Diese sind im  speziellen die get- und set-Methoden sowie
*	die Klonfunktion zur weiteren Verarbeitung in der Datenbank.
*
*	@author Dennis, Rene, Benjamin, Fritz, Michael
*	@version 1.1
*/


public class MuFi implements Cloneable, Comparable<MuFi>{
	/**
	 * ist der Pfad in dem das MusicFile liegt 
	 * HasGetter
	 * HasSetter
	 */
	private String path;
	/**
	 * ist die ID des MusicFiles 
	 * HasGetter
	 * HasSetter
	 */
	private int id;
	/**
	 * ist der Titel des MusicFiles 
	 * HasGetter
	 * HasSetter
	 */
	private String title;
	/**
	 * ist der Urheber des MusicFiles  
	 * HasGetter
	 * HasSetter
	 */
	private String artist;
	/**
	 *ist das Album in dem sich der Titel befindet 
	 * HasGetter
	 * HasSetter
	 */
	private String album;
	/**
	 *ist das Genre des MusicFiles
	 * HasGetter
	 * HasSetter
	 */
	private String genre;
	/**
	 * ist der Preise des MusicFiles  
	 * HasGetter
	 * HasSetter
	 */
	private float price;
	
	private final static Database DATABASE = new Database();
	
	/**
	 * Konstruktor fuer ein MusicFile, welches gleich mit allen uebergebenen Werten initialisiert wird 
	 * @param id ist die eindeutige Kennung des Musictitels
	 * @param title ist der Name des Titels 
	 * @param artist ist der Interpret des Titels 
	 * @param album ist der Name des Albums auf dem der Titel zu finden ist 
	 * @param genre ist die Stilrichtung in die das Musicfile eingeordnet wird 
	 * @param price ist der Preis des Titels in unserem MusicStore
	 */
	public MuFi(final int id, final String title, final String artist, final String album, 
				final String genre, final float price) {
		this.id = id;
		this.title = title;
		this.artist = artist;
		this.album = album;
		this.genre = genre;
		this.price = price;
		this.path = "/ILoveMusic/" + artist + "/" + album + "/" + title + ".mp3";
	}
	/**
	/**
	 * Konstruktor fuer ein MusicFile welches gleich mit allen uebergebenen Werten initialisiert wird 
	 * @param title ist der Name des Titels 
	 * @param artist ist derjenige der den Titel erzeugt hat 
	 * @param album ist der Name des Albums auf dem der Titel zu finden ist 
	 * @param genre ist die Stil Richtung in die das Musicfile eingeordnet wird 
	 * @param price ist der Preis des Titels in unserem MusicStore zu dem der Titel angeboten wird 
	 */
	public MuFi(String title, String artist, String album, String genre, float price) {
		this.id =  DATABASE.createFileId();
		this.title = title;
		this.artist = artist;
		this.album = album;
		this.genre = genre;
		this.price = price;
		this.path = "/ILoveMusic/" + artist + "/" + album + "/" + title +".mp3";
		//db.insertMuFi(this);
	}
		
	/**
	 * Copy, zum erzeugen einer Kopie eines Objekts, fuer die weitere Datenbankverarbeitung
	 * @throws CloneNotSupportedException 
	 */
	@Override
	public MuFi clone() throws CloneNotSupportedException {
		MuFi musicfile = (MuFi) super.clone();
		musicfile.id =  this.id;
		musicfile.title = this.title;
		musicfile.artist = this.artist;
		musicfile.album = this.album;
		musicfile.genre = this.genre;
		musicfile.price = this.price;
		musicfile.path = this.path;
		return musicfile;
	}
	
	//get-Methoden
	
	public int getId() {
		return this.id;
	}	
	public String getTitle() {
		return this.title;
	}	
	public String getArtist() {
		return this.artist;
	}	
	public String getAlbum() {
		return this.album;
	}	
	public String getGenre() {
		return this.genre;
	}	
	public float getPrice() {
		return this.price;
	}
	public String getPath(){
		return this.path;
	}
	
	
	// set-Methoden
	// eventuell Il
	
	public void setTitle(String newTitle) {
		if (newTitle == null)
			throw new IllegalArgumentException("leere Eingabe");
		this.title = newTitle;
	}
	
	public void setArtist(String newArtist) {
		if (newArtist == null)
			throw new IllegalArgumentException("leere Eingabe");
		
		this.artist = newArtist;
	}
	
	public void setAlbum(String newAlbum) {
		if (newAlbum == null)
			throw new IllegalArgumentException("leere Eingabe");
		
		this.album = newAlbum;
	}
	
	public void setGenre(String newGenre) {
		if (newGenre == null)
			throw new IllegalArgumentException("leere Eingabe");
		
		this.genre = newGenre;
	}
	
	public static MuFi getById(int id) {
		return DATABASE.getMuFi(id);
	}
	
	public void setPrice(float newPrice) {
		this.price = newPrice;
	}
	/**
	 * vergleicht das aktuelle MusicFile mit einem anderen  
	 */
	@Override
	public int compareTo(MuFi o) {
		return ((Integer) this.id).compareTo(o.id);
	}
	/**
	 * fuegt die Informationen des MusicFiles in einen konvertierten String
	 */
	public String toString() {
		return "[" + this.id + ":" + this.title + "," + this.artist + "]";
	}
	
	public static void main(String[] args) {
		MuFi m1 = new MuFi(1, "title", "artist", "album", "genre", 1.1f);
		MuFi m2 = new MuFi(1, "title1", "artist1", "album1", "genre1", 12.1f);
		System.out.print(m1.compareTo(m2));
	}
}

package base;
import java.util.*;

/**
 * Diese Klasse stellt Funktionen bereit,die 
 * fuer den Einkauf von Musiktiteln benoetigt werden.
 * 
 * @author Dennis, Rene, Benjamin, Fritz, Michael
 * @version 1.1
 */


public class ShoppingCart {

	private transient final Set<MuFi> cart = new TreeSet<MuFi>();
	private static Database database = new Database();

	/**
	 * Ist die Suchfunktion, die einen Titel in der Datenbank sucht  
	 * @param searchString ist der eingegebene Suchstring
	 * @return results ist die MuFi Liste mit den den Suchergebnissen
	 */
	public static List<MuFi> search(final String searchString) {
		//Iterator for-Schleife
		//Fkt loescht alle Eintraege die *nicht* zu dem searchString passen
		//Die uebrigbleibende Liste enthaelt die Treffer
		final String lowerSearchString = searchString.toLowerCase(Locale.GERMAN);
		final List<MuFi> results = new ArrayList<MuFi>();
		for (MuFi file:database.getMuFis()) {
			if(	(file.getArtist().toLowerCase().indexOf(lowerSearchString) > -1) || 
				(file.getTitle().toLowerCase().indexOf(lowerSearchString) > -1)  ||
				(file.getAlbum().toLowerCase().indexOf(lowerSearchString) > -1)  ||
				(file.getGenre().toLowerCase().indexOf(lowerSearchString) > -1)) {
			    results.add(file);
			}
		}
		return results;
	}

	/**
    * Zeigt den aktuellen Stand das Warenkorbs dar 
    * @return Liste von MusicFiles die aktuell im Warenkorb sind
    */
	public Set<MuFi> showcart() {
		return cart;
	}
	
	/**
	 * ueberprueft den Warenkorb ob dieser leer ist
	 * @return true das Ergebnis von isEmty
	 */
	public boolean checkEmpty() {
		return cart.isEmpty();
	}
	
	/**
	 * Packt alle MuFis aus der Datenbank in eine Liste 
	 * @return Liste aller MusicFiles
	 */
	public static Set<MuFi> getAllMuFis() {
		return database.getMuFis();
	}
	
	
	/**
	 * fuegt einen Titel in den Warenkorb
	 * @param mid - ID des hinzuzufuegenden Titels
	 */
	public boolean addTitleToCart(final int mid) {
		cart.add(database.getMuFi(mid));
		return true;
	}


	/**
	 * loescht einen bestimmten Titel aus dem Warenkorb 
	 * @param mid - ID des zu loeschenden Titels
	 */
	public void deleteTitleFromCart(final int mid) {

		//Iterator for-schleife
		for (MuFi file:cart) {
			if (file.getId() == mid) {
				cart.remove(file);
				break;
			}
		}
	}

	/**
	 * leert den Warenkorb volltaendig 
	 * @return true wenn die Methode erfolgreich ausgefuehrt wurde 
	 */
	public boolean emptyCart() {
		cart.clear();
		return true;
	}

	/**
	 * Getter wie viele Titel der Warenkorb enthaelt
	 * @return Anzahl der Titel im Warenkorb
	 */
	public int getSize() {
		return cart.size();
	}
	
	
	
	/**
	 * Methode um den Warenkorb zu bezahlen und so die Titel dem Profil hinzuzufuegen 
	 * @return False, falls die verfuegbaren Credits nicht reichen um alle Titel zu bezahlen
	 * 			True sonst.
	 */
	public boolean payCart(final Profile user) {
		final float currentCredits = user.getUserCredit();
		final float newCredits = currentCredits - getTotalPrice();
		final List<MuFi> titleList = user.getUserTitleList();
		if (newCredits < 0) {
			return false;
		} else {
			user.setUserCredit(newCredits);
			user.getUserTitleList().addAll(cart);
			for(MuFi m : cart) {
				if(titleList.contains(m)) {
					
				} else {
					user.setUserCredit(user.getUserCredit()-m.getPrice());
					user.addTitelEndToTitleList(m);
				}
			}
			cart.clear();
			return true;	
		}		
	}

	/**
	 * Berechnet den Gesammtpreis der im Warenkorb befindlichen Titel
	 * @return Gesamtpreis aller sich im Warenkorb befindlichen Titel
	 */
	private float getTotalPrice() {

		float totalPrice = 0.0f;
		for (MuFi file:cart) {
			totalPrice += file.getPrice();
		}
		return totalPrice;
	}	
}

package base;

import base.exceptions.NegativeNumberException;

/**
 * @author Dennis, Rene, Fritz, Benjamin, Michael 
 * @version 1.1
 *
 * Description: Diese Klasse enthael alle benoetigten Methoden und Konstrucktoren um einen UserCredit zu initialisieren, ihn zu 
 * addieren oder subtrahieren sowie um sich die Werte ausgeben zu lassen.Außerdem die Klonfunktion zur 
 * weiteren Verarbeitung in der Datenbank
 *
 */
public class Credit implements Cloneable{

	private transient float balance;
	
	/**
	 * Konstruktor fuer Credit(Credit ist das Guthaben eines Users und wird im Profil abgespeichert)  
	 * @param val CreditWert wird mit uebergebenen Val initialisiert
	 */
	public Credit(final float val) {
		this.balance = val;
	}
	
	/**
	 * Konstruktor für Credit, wobei der Wert dieses mal mit 0 initialisiert wird 
	 */
	public Credit() {
		this.balance = 0;
	}
	
	/**
	 * Copy zum erzeugen einer Kopie eines Objekts fuer die weitere Datenbankverarbeitung
	 * @throws CloneNotSupportedException 
	 */
	@Override
	public Credit clone() throws CloneNotSupportedException {
		final Credit credit = (Credit) super.clone();
		credit.balance = this.balance;
		return credit;
	}
	
	public float getCredit() {
		return this.balance;
	}
	
	/**
	  *  addiert einen Wert zum Credit des Profile's
	  *  
	  *  @param 	value Wert mit dem addiert werden soll
	  *  @throws 	NegativeNumberException Wirft eine Exeption wenn versucht wird einen Negativen Wert zu addieren
	  */
	public void addCredits(final float value) throws NegativeNumberException {
		
		if(value < 0){
			throw new NegativeNumberException("do not use negative values");
		}
		else{
			this.balance = value + this.balance;
		}
	}
	
	/**
	 * subtrahiert einen Wert vom Credit des aktuellen Profile's
	 * @param 	value 
	 * @throws 	NegativeNumberException Wirft eine Exeption wenn versucht wird einen Negativen 
	 * 			Wert zu suptrahieren, oder wenn der 
	 * 			User credit droht unter null fallen nach der Subtraktion 
	 */
	public void subCredits(final float value) throws NegativeNumberException {
		
		if(value < 0){
			throw new NegativeNumberException("do not use negative values");
		}
		else{
			if(this.balance < 0 ){
				throw new NegativeNumberException("credit value under zero");
			}
			else{
				this.balance = this.balance - value;
			}
		}
	}
}

package base;

/**
 * 
 * @author Dennis, Rene, Benjamin, Fritz, Michael
 *Description: enthaelt die Rollen dioe ein Nutzer im Profil annehmen kann
 */
public enum Role {
	user,
	admin
}

package base;

/**
 * @author Dennis, Rene, Benjamin, Fritz, Michael
 * @version 1.1
 *
 * Description: stellt die benoetigten Passwort Methoden aendern,ueberpruefen und initialisieren bereit, 
 * wirft NotEqualException bei Fehler
 */

public class Password implements Cloneable {

		private String password;
		
		/** 
		 * Konstruktor fuer das Passwort (aus praktischen Gründen wurde hier ein String gewählt,
		 * normalerweise wird hier die Verwendung einer Hash-Funktion empfohlen)
		 * @param pass ist gewuenschte passwort
		 */
		public Password(String pass) {
			this.password = pass;
		}
		
		/**
		 * 	
		 * Copy zum erzeugen einer Kopie eines Objekts fuer die weitere Datenbankverarbeitung
		 * @throws CloneNotSupportedException 
		 */
		public Password clone() throws CloneNotSupportedException {
			Password pass = (Password) super.clone();
			pass.password = this.password;
			return pass;
		}
		
		/**
		 * Eine Methode um ein Passwort zu aendern 
		 * @param pass ist das gewuenschte neue Passwort
		 * @throws NotEqualException wenn das oldPasswort nicht mit dem aktuellen Passwort des Profils ueber einstimmmen
		 */
		public void changePassword(String pass) {
			this.password = pass;
		}
		
		/**
		 * prueft ob das angegebene Passwort das aktuelle Passwort ist 
		 * @param pass ist das zu pruefende Passwort 
		 * @return true wenn newPasswort mit dem Alten uebereinstimmmt, ansonsten false  
		 */
		public boolean checkPassword(String pass) {
			if(pass != "")
			return this.password.equals(pass);
			else
				return false;
		}
		
		/**
		 * Cast des Passwortes in einen String
		 */
		public String toString() {
			return this.password;
		}
}

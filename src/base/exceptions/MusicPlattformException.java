package base.exceptions;

@SuppressWarnings("serial")
public abstract class MusicPlattformException extends Exception{
	public MusicPlattformException() {
		super("MusicPlattformException");
	}
	
	public MusicPlattformException(String msg) {
		super(msg);
	}
}

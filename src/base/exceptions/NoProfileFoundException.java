package base.exceptions;

@SuppressWarnings("serial")
public class NoProfileFoundException extends MusicPlattformException{
	public NoProfileFoundException() {
		super("No User Found");
	}
}

package base.exceptions;
/**
 * 
 * @author dennis
 *beschreibt ein Fehlverhalten von Vergleichen
 *
 */

@SuppressWarnings("serial")
public class NotEqualException extends Exception{
	
	public NotEqualException(String s) {
		super(s);
	}

}


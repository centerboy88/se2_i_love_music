package base.exceptions;

@SuppressWarnings("serial")
public class IncorrectPasswordException extends MusicPlattformException{
	public IncorrectPasswordException() {
		super("Incorrect Password");
	}
}

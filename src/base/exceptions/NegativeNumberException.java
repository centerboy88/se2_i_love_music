package base.exceptions;

/**
 * @author dennis
 *
 *bescheibt ein negatives nicht erlaubtes Zahlenformat
 */


@SuppressWarnings("serial")
public class NegativeNumberException extends Exception {
	
	public NegativeNumberException(String s){
		super(s);
	}

}

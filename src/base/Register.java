package base;

/**

 * @author Dennis, Rene, Benjamin, Fritz, Michael
 * Description: generiert einen neuen Benutzer und sendet eine Verifikationsmail zur Aktivierung des Accounts
 * ueberprueft das Vorhandensein des Usernames, wenn schon vorhanden wird null zurueckgegeben
 *
 */
public final class Register {
	
	private final static Database DATABASE = new Database();
	
	private Register() {};
	
	public static Profile createProfile(final String username, final String pass, final String email) {
		if(DATABASE.userNameExist(username)){
			return null;
		} else {
			final Profile profile = new Profile(username, email, pass);
			DATABASE.insertProfile(profile);
			new Email(profile);
			return profile;
		}
	}	
}

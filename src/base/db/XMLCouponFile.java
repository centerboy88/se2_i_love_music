package base.db;

import java.io.File;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import base.Coupon;
import base.MuFi;
import base.Profile;
 
public class XMLCouponFile {
 
	public static Set<Coupon> read() {
		
		Set<Coupon> coupons = new TreeSet<Coupon>();
 
	    try {
	    	//System.out.println(new File(".").getAbsoluteFile());
			File fXmlFile = new File("./bin/base/db/coupons.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
		 
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
		 
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		 
			NodeList nList = doc.getElementsByTagName("Coupon");
		 
			for(int temp = 0; temp < nList.getLength(); temp++) {
				
		 
				Node nNode = nList.item(temp);
		 
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());
		 
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) nNode;
					String code 	= eElement.getElementsByTagName("Code").item(0).getTextContent();
					float value 	= Float.parseFloat(eElement.getElementsByTagName("Value").item(0).getTextContent());
					boolean valid 	= Boolean.parseBoolean(eElement.getElementsByTagName("Valid").item(0).getTextContent());
					Coupon coupon = new Coupon(value, code, valid);
					coupons.add(coupon);
				}
			}
			
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    return coupons;
	}
	
	public static void write(Set<Coupon> coupons) {
		 
		  try {
	 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("Coupons");
			doc.appendChild(rootElement);
			for(Coupon c : coupons) {
				// staff elements
				Element coupon = doc.createElement("Coupon");
				rootElement.appendChild(coupon);
		 
				// shorten way
				// staff.setAttribute("id", "1");
		 
				// firstname elements
				Element code = doc.createElement("Code");
				code.appendChild(doc.createTextNode(c.getCode()));
				coupon.appendChild(code);
				
				Element value = doc.createElement("Value");
				value.appendChild(doc.createTextNode(Float.toString(c.getValue())));
				coupon.appendChild(value);
				
				Element valid = doc.createElement("Valid");
				valid.appendChild(doc.createTextNode(Boolean.toString(c.isValid())));
				coupon.appendChild(valid);
			}
			
	 
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("./bin/base/db/coupons.xml"));
	 
			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);
	 
			transformer.transform(source, result);
	 
			//System.out.println("File saved!");
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		}
	
	public static void main(String[] args) {
		Set<Coupon> cs = read();
		write(cs);
	}
}

package base.db;

import java.io.File;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import base.Database;
import base.MuFi;
 
public class XMLMusicFile {
	
	private static Database db = new Database();
 
	public static Set<MuFi> read() {
		
		Set<MuFi> musicfiles = new TreeSet<MuFi>();
 
	    try {
	    	//System.out.println(new File(".").getAbsoluteFile());
			File fXmlFile = new File("./bin/base/db/musicfiles.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
		 
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
		 
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		 
			NodeList nList = doc.getElementsByTagName("MusicFile");
		 
			for(int temp = 0; temp < nList.getLength(); temp++) {
				
		 
				Node nNode = nList.item(temp);
		 
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());
				/*
				 * 	<Profile id="2">
						<Username>dennis</Username>
						<Email>dennis@inet.de</Email>
						<Password>1234</Password>
						<Balance>500</Balance>
						<Titlelist>
							<id>1</id>
							<id>2</id>
							<id>3</id>
						</Titlelist>
					</Profile>
				 */
		 
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) nNode;
					
					int id = Integer.parseInt(eElement.getAttribute("id"));
					String title 	= eElement.getElementsByTagName("Title").item(0).getTextContent();
					String artist 	= eElement.getElementsByTagName("Artist").item(0).getTextContent();
					String album 	= eElement.getElementsByTagName("Album").item(0).getTextContent();
					String genre 	= eElement.getElementsByTagName("Genre").item(0).getTextContent();
					float price 		= Float.parseFloat(eElement.getElementsByTagName("Price").item(0).getTextContent());

					
					MuFi musicfile = new MuFi(id, title, artist, album, genre, price);
					musicfiles.add(musicfile);
				}
			}
			
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    return musicfiles;
	}
	
	public static void write(Set<MuFi> musicfiles) {
		 
		  try {
	 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("MusicFiles");
			doc.appendChild(rootElement);
			for(MuFi m : musicfiles) {
				// staff elements
				Element musicfile = doc.createElement("MusicFile");
				rootElement.appendChild(musicfile);
		 
				// set attribute to staff element
				Attr attr = doc.createAttribute("id");
				attr.setValue(Integer.toString(m.getId()));
				musicfile.setAttributeNode(attr);
		 
				// shorten way
				// staff.setAttribute("id", "1");
		 
				// firstname elements
				Element title = doc.createElement("Title");
				title.appendChild(doc.createTextNode(m.getTitle()));
				musicfile.appendChild(title);
				
				Element artist = doc.createElement("Artist");
				artist.appendChild(doc.createTextNode(m.getArtist()));
				musicfile.appendChild(artist);
				
				Element album = doc.createElement("Album");
				album.appendChild(doc.createTextNode(m.getAlbum()));
				musicfile.appendChild(album);
				
				Element genre = doc.createElement("Genre");
				genre.appendChild(doc.createTextNode(m.getGenre()));
				musicfile.appendChild(genre);
				
				Element price = doc.createElement("Price");
				price.appendChild(doc.createTextNode(Float.toString(m.getPrice())));
				musicfile.appendChild(price);
			}
			
	 
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("./bin/base/db/musicfiles.xml"));
	 
			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);
	 
			transformer.transform(source, result);
	 
			//System.out.println("File saved!");
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		}
	
	public static void main(String[] args) {
		Set<MuFi> ms = read();
		write(ms);
	}
}

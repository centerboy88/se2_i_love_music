package base.db;

import java.io.File;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import base.Credit;
import base.Database;
import base.MuFi;
import base.Profile;
import base.Role;
 
public class XMLProfileFile {
	
	private static Database db = new Database();
 
	public static Set<Profile> read() {
		Set<Profile> profiles = new TreeSet<Profile>();
	    try {
	    	//System.out.println(new File(".").getAbsoluteFile());
			File fXmlFile = new File("./bin/base/db/profiles.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
		 
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
		 
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		 
			NodeList nList = doc.getElementsByTagName("Profile");
		 
			for(int temp = 0; temp < nList.getLength(); temp++) {
//				System.out.println("Profil" + temp);
				Node nProfile = nList.item(temp);
		 
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());
		 
				if (nProfile.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eProfile = (Element) nProfile;
					int id = Integer.parseInt(eProfile.getAttribute("id"));
					String username = eProfile.getElementsByTagName("Username").item(0).getTextContent();
					String email 	= eProfile.getElementsByTagName("Email").item(0).getTextContent();
					String password = eProfile.getElementsByTagName("Password").item(0).getTextContent();
					Credit credit 	= new Credit(Float.parseFloat(eProfile.getElementsByTagName("Balance").item(0).getTextContent()));
					boolean status = Boolean.parseBoolean((eProfile.getElementsByTagName("Status").item(0).getTextContent()));
					String roleString = eProfile.getElementsByTagName("Role").item(0).getTextContent();
					Role userRole;
					if(roleString.equals("admin")) {
						userRole = Role.admin;
					} else {
						userRole = Role.user;
					}
					
					
					TreeSet<MuFi> titleSet = new TreeSet<MuFi>();
					NodeList titles = eProfile.getElementsByTagName("TitleId");
					for(int ii = 0; ii < titles.getLength(); ii++) {			
						try {
							int mid = Integer.parseInt(titles.item(ii).getTextContent());						
							MuFi m = db.getMuFi(mid);
							titleSet.add(m);
						} catch (Exception e) {
//							System.out.println("Fehler beim XML-Music auslesen:" + e.getMessage());
						}
					}
					
//					ArrayList<MuFi> playlist = new ArrayList<MuFi>();
//					NodeList playTitles = eProfile.getElementsByTagName("PlaylistTitleId");
//					System.out.println("Länge: " + playTitles.getLength());
//					for(int ii = 0; ii < playTitles.getLength(); ii++) {			
//						try {
//							int mid = Integer.parseInt(playTitles.item(ii).getTextContent());
//							MuFi m = db.getMuFi(mid);
//							playlist.add(m);
//							System.out.println(playlist.toString());
//						} catch (Exception e) {
//							System.out.println("Fehler beim XML-Play auslesen:" + e.getMessage());
//						}
//					}
					ArrayList<MuFi> titlelist = new ArrayList<MuFi>();
					titlelist.addAll(titleSet);
					
					Profile profile = new Profile(id, username, email, password, credit, status, userRole, titlelist, new ArrayList<MuFi>());
					profiles.add(profile);
				}
			}
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    return profiles;
	}
	
	public static void write(Set<Profile> profiles) {
		  try {
	 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("Profiles");
			doc.appendChild(rootElement);
			for(Profile p : profiles) {
				// staff elements
				Element profile = doc.createElement("Profile");
				rootElement.appendChild(profile);
		 
				// set attribute to staff element
				Attr attr = doc.createAttribute("id");
				attr.setValue(Integer.toString(p.getUid()));
				profile.setAttributeNode(attr);
		 
				// shorten way
				// staff.setAttribute("id", "1");
		 
				// firstname elements
				Element uname = doc.createElement("Username");
				uname.appendChild(doc.createTextNode(p.getUserName()));
				profile.appendChild(uname);
				
				Element email = doc.createElement("Email");
				email.appendChild(doc.createTextNode(p.getUserEmail()));
				profile.appendChild(email);
				
				Element password = doc.createElement("Password");
				password.appendChild(doc.createTextNode(p.getPassword().toString()));
				profile.appendChild(password);
				
				Element balance = doc.createElement("Balance");
				balance.appendChild(doc.createTextNode(Float.toString((p.getUserCredit()))));
				profile.appendChild(balance);
				
				Element status = doc.createElement("Status");
				status.appendChild(doc.createTextNode(Boolean.toString((p.getUserStatus()))));
				profile.appendChild(status);
				
				Role roleElement = p.getUserRole();
				String roleString;
				if(roleElement == Role.admin) {
					roleString = "admin";
				} else {
					roleString = "user";
				}
				Element role = doc.createElement("Role");
				role.appendChild(doc.createTextNode(roleString));
				profile.appendChild(role);
				
				Element titles = doc.createElement("Titlelist");
				for(MuFi m: p.getUserTitleList()) {
					Element title = doc.createElement("TitleId");
					title.appendChild(doc.createTextNode(Integer.toString(m.getId())));
					titles.appendChild(title);
				}
				profile.appendChild(titles);
			}
			
	 
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("./bin/base/db/profiles.xml"));
	 
			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);
	 
			transformer.transform(source, result);
	 
			//System.out.println("File saved!");
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
	}
	
	public static void main(String[] args) {
		db.print();
		Set<Profile> ps = read();
		write(ps);
		db.print();
	}
}

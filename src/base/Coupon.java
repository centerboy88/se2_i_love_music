package base;
import java.util.Random;
import java.util.Set;

/**
 * 
 * @author Dennis, Rene, Fritz, Benjamin, Michael 
 * @version 1.1
 * Description: diese Klasse enthaelt alle Methoden um Gutscheine zu erstellen, zu entwerten 
 * und Werte auszulesen bzw auch neu zusetzen, sowie die Klonfunktion zur weiteren Verarbeitung in der Datenbank
 */
public class Coupon implements Cloneable, Comparable<Coupon>{
	
	private static Database database = new Database();
	private String code;
	private float value;
	private transient boolean valid;
	
	private static final char[] CHARS = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

	/**
	 * Konstruktor für Coupon, der mit Standardwerten initialisiert wird
	 * Code wird genereriert und auf gültig gesetzt
	 * @param value ist der zu belegende Wert
	 */
	public Coupon(final float value) {
		this.code = generateCode();
		this.value = value;
		this.valid = true;
	}
	
	/**
	 * Konstruktor für Coupon der mit folgenden Werten initialisiert wird 
	 * @param value ist der Wert des Gutscheins 
	 * @param code ist der Code des Gutscheins
	 * @param valid ist der Status des Gutscheins( true= kann genutzt werden, false= verbraucht)
	 */
	public Coupon(final float value, final String code, final boolean valid) {
		this.code = code;
		this.value = value;
		this.valid = valid;
	}

	/**
	 * Copy zum Erzeugen einer Kopie eines Objekts fuer die weitere Datenbankverarbeitung
	 * 
	 */
	@Override
	public Coupon clone() throws CloneNotSupportedException{
		final Coupon coupon = (Coupon) super.clone();
		coupon.value = this.value;
		coupon.code = this.code;
		coupon.valid = this.valid;
		return coupon;
	}
	
	/**
	 * generiert eine Anzahl von Gutscheinen mit einem Gewissen Wert und speichert Sie in der Datenbank
	 * @param value : Wert von jeden erstellten Gutschein
	 * @param amount : Anzahl der erstellten Gutscheine 
	 * @return Wenn die Methode erfolgreich war, wird ein true zurückgegeben 
	 */
	public static boolean generateCoupons(final float value, final int amount){
		Coupon coupon;
		for(int i=0; i < amount; i++) {
			coupon = new Coupon(value);
			database.insertCoupon(coupon);
		}
		return true;
	}
	
	/**
	 * Entwertet den Coupon und addiert Wert auf das Profilguthaben, akutalisiert Datenbank 
	 * @param profile Ist das Profil, welches den Gutschein einlöst und dessen Guthaben erhöt wird
	 * @param code ist der Code des Gutscheins der eigelöst wird, und danach auch entwertet wird
	 */
	public static void voidCoupon(final Profile profile, final String code) {
		final Coupon coupon = database.getCoupon(code);
		profile.setUserCredit(profile.getUserCredit() + coupon.value);
		coupon.valid = false;
		database.setCoupon(coupon);
	}
	
	/**
	 */
	public static boolean exists(String couponCode) {
		return (database.getCoupon(couponCode) != null);
	}
	
	public static Coupon getByCode(String couponCode) {
		return database.getCoupon(couponCode);
	}
	
	/**
	 * Getter für Value
	 * @return Gibt den Wert des aktuellen Gutscheins zurück
	 */
	public float getValue() {
		return this.value;
	}
	
	/**
	 * Getter für Code
	 * @return gibt den Gutscheincode des aktuellen Gutscheins zurueck
	 */
	public String getCode() {
		return this.code;
	}
	
	/** 
	 * gibt an ob ein Gutschein noch gueltig ist 
	 * @return aktueller Status des Gutscheins
	 */
	public boolean isValid() {
		return this.valid;
	}
	
	/**
	 * Generiert 5 stellige die Gutschein Codes zufaellig aus Zahlen und Buchstaben die fuer die Gutscheine.
	 * Außerdem wird verhindert das gleiche Codes erstellt werden  
	 * @return ein String mit dem erstellten Gutscheincode
	 */
	private String generateCode() {
		final StringBuilder varString = new StringBuilder();
		final Random random = new Random();
		//code-Laenge 5
		do {
			for (int i = 0; i < 5; i++) {
				varString.append(CHARS[random.nextInt(CHARS.length)]);
			}
		} while(database.couponExists(varString.toString()));
		return varString.toString();
	}
	
	/**
	 * Getter für alle Coupons
	 * @return eine Liste mit allen Coupons aus der Datenbank 
	 */
	public static Set<Coupon> getAll() {
		return database.getCoupons();
	}
	
	/**
	 * methode um die Darstellung in der GUI zu vereinfachen und die Couponliste besser darstellen zu können.
	 * @param coupons Ist die Liste an Coupons die dargestellt werden soll
	 * @return Ein Array mit dem Couponcode in der ersten Spalte, dem Couponwert in der zweiten Spalte und dem Status des Coupons
	 * in der dritten Spalte 
	 */
	public static Object[][] conv(final Set<Coupon> coupons) {
		int rowIndex = 0;
		Object[][] results = new Object[coupons.size()][3];
		for(Coupon c:coupons) {
			results[rowIndex][0] = c.code;
			results[rowIndex][1] = c.value;
			results[rowIndex][2] = Boolean.valueOf(c.valid);
			rowIndex++;
		}
		return results;
	}
	
	/**
	 * fügt den Gutscheincode und den Wert geordnet in einen String 
	 */
	@Override
	public String toString() {
		return "[" + this.code + "," + this.value + "]";
	}
	
	/**
	 * vergleicht zwei Coupons, als Reverenz dient der Gutscheincode  
	 */
	@Override
	public int compareTo(final Coupon coupon) {
		return this.code.compareTo(coupon.code);
	}
	
}

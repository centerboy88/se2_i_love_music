package base;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import base.exceptions.IncorrectPasswordException;
import base.exceptions.NoProfileFoundException;
import base.exceptions.NotEqualException;
import base.exceptions.NotFoundException;


/**
 * Die Klasse MusicPlattform stellt die Schnittstelle zur GUI bereit. Alle Funktionen, die
 * ueber die GUI aufgerufen werden, gehen ueber MusicPlattform und werden weitergeleitet an
 * die jeweiligen Klassen in der Hierachie.
 * 
 * @author Dennis, Rene, Benjamin, Fritz, Michael
 * @version 1.1
 */


public class MusicPlattform {
	private Profile user = null;
	private ShoppingCart cart = new ShoppingCart();
	
	private static final Logger LOG = Logger.getLogger(MusicPlattform.class.getName());
	
	/** 
	 * Konstruktor der die Musikplatform Initiiert 
	 */
	public MusicPlattform() {
		LOG.info("Musikplattform initialisiert.");
	}

	/**
	 * ladt die aktuellsten Daten des jeweiligen Profils
	 */
	public void updateData() {
		if(this.user != null) {
			try {
				this.user = Profile.getProfile(this.user.getUserName());
			} catch (NoProfileFoundException e) {
				LOG.log(Level.WARNING, "Fehler: ", e);
			}
		}
	}
	
	/**
	 * Funktion die den User in das System einloggen soll
	 * @param username ist der Names des Users der sich einloggen will
	 * @param pass ist das Passwort des Users 
	 */
	public void login(final String username, final char[] pass) throws IncorrectPasswordException, NoProfileFoundException{
		new Database().print();
		final Profile profile = Profile.getProfile(username);
		final String password = new String(pass);	//char[] to String cast
		if(profile.checkPassword(password)) {
			LOG.info("Passwort akzeptiert");
			this.user = profile;
		} else {
			LOG.info("Falsches Passwort");
			this.user = null;
			throw new IncorrectPasswordException();
		}
	}
	
	/**
	 * loggt den zur Zeit angemeldeten User wieder aus
	 */
	public void logout() {
		LOG.info("User logged out");
		this.user = null;
	}
	
	
	/** 
	 * registriert einen User im System und legt einen neuen Datenbankeintrag an  
	 * @param username ist der name des zu registrierenden Users
	 * @param pass ist das Passwort des zu registrierenden Users
	 * @param email ist die Emailadresse das zu regeitrierenden Users
	 */	
	public void register(final String username, final String pass, final String email) {
		//System.out.println("Anlegen eines neuen Profils.");
		LOG.info("Anlegen eines neuen Profils");
		this.user = Register.createProfile(username, pass, email);
	}
	
	
	/**
	 * Erstellt eine Konvertierung von MuFi Listen fuer die GUI
	 * 
	 * @param MuFiList ist die zu konvertierende Liste 
	 * @return zweidimensionales Arrays mit Inhalt von MuFis
	 */
	public static Object[][] listObjectArrayConverter(final List<MuFi> MuFiList) {
		final int columnCount = 7;		//id, artist, title, album, genre, price, boolean (kaufen/loeschen)
		int index = 0;
		Object[][] results = new Object[MuFiList.size()][columnCount];
		for(MuFi file:MuFiList) {
			results[index][0] = file.getId();
			results[index][1] = file.getTitle();
			results[index][2] = file.getArtist();
			results[index][3] = file.getAlbum();
			results[index][4] = file.getGenre();
			results[index][5] = file.getPrice();
			results[index][6] = Boolean.FALSE;
			index++;
		}
		return results;
	}
	/**
	 * Erstellt eine Konvertierung von MuFi Sets  fuer die GUI
	 * @param musicfiles ist ein Set von MusicFiles 
	 * @return zweidimensionales Arrays mit Inhalt von MuFis
	 */
	public static Object[][] setObjectArrayConverter(final Set<MuFi> musicfiles) {
		final int columnCount = 7;		//id, artist, title, album, genre, price, boolean(kaufen/loeschen)
		int index = 0;
		Object[][] results = new Object[musicfiles.size()][columnCount];
		for(MuFi file:musicfiles) {
			results[index][0] = file.getId();
			results[index][1] = file.getTitle();
			results[index][2] = file.getArtist();
			results[index][3] = file.getAlbum();
			results[index][4] = file.getGenre();
			results[index][5] = file.getPrice();
			results[index][6] = Boolean.FALSE;
			index++;
		}
		return results;
	}
	
	/**
	 * Erstellt eine Konvertierung von Profil Sets fuer die GUI
	 * @param profiles ist das Set von Profilen
	 * @return zweidimensionales Arrays mit Inhalt von Profilen 
	 */
	public static Object[][] setObjectArrayConverterProfiles(final Set<Profile> profiles) {
		final int columnCount = 5;
		int index = 0;
		Object[][] results = new Object[profiles.size()][columnCount];
		for(Profile p:profiles) {
			results[index][0] = p.getUid();
			results[index][1] = p.getUserName();
			results[index][2] = p.getUserCredit();
			results[index][4] = Boolean.FALSE;
			if(p.getUserRole() == Role.admin) {
				results[index][3] = Boolean.TRUE;
			} else {
				results[index][3] = Boolean.FALSE;
			}
			index++;
		}
		return results;
	}
	
	
	/**
	 * Sucht einen Titel in der Datenbank
	 * 
	 * @param title ist der zu suchende Musiktitel
	 * @return Liste mit MusicFiles 
	 */		
	public static List<MuFi> searchTitle(String title) {
		 return ShoppingCart.search(title);
	}
	
	
	/**
	 * Zeigt den Einkaufswagen
	 * 
 	 * @return den Inhalt des Warenkorbs
	 */	
	public Set<MuFi> showTitleToBuy() {
		return cart.showcart();
	}
	/**
	 * gibt die Anzahl der MusicFiles im Warenkorb aus
	 * @return Anzahl von MusicFiles
	 */
	public int getCartSize() {
		return cart.getSize();
	}
	/**
	 * ueberprueft ob der Warenkorb leer ist 
	 * @return das ergbnis von checkEmpty
	 */
	public boolean isCartEmpty() {
		return cart.checkEmpty();
	}
	/**
	 * leert den Warenkorb
	 */
	public void clearCart() {
		this.cart.emptyCart();
	}
	
	/**
	 * Benutzen eines Gutscheins , entwertet den Gutschein und schreibt es dem Profilguthaben gut
	 * 
	 * @param profile ist das Profil dem das Guthaben hinzugefuegt werden soll
	 * @param code ist der Code des Gutschein der benutzt werden soll 
	 * @return true wenn abgeschlossen
	 */
	public boolean useCouponCode(final Profile profile, final String code) {
		
		Coupon.voidCoupon(profile,code);
		return true;
	}
	
	
	/**
	 * Erstellt eine Anzahl von Gutscheinen mit dem Wert Value und speichert Sie in der Datenbank
	 * 
	 * @param value ist der Wert der anzulegenden Gutscheine
	 * @param amount ist die Anzahl der zu erstellenden Gutscheine
	 * @return true falls erfolgreich
	 */
	public boolean createCouponCode(final int value, final int amount) {
		Coupon.generateCoupons(value,amount);
		return true;
	}
	
	
	/**
	 * schaut in der Titelliste des User nach und gibt einen Downloadpfad zurueck
	 * @param element Ist das gewuenschte Objekt welches gedownloadet werden soll
	 * @return den Pfad an dem der MusicTitel zufinden ist 
	 * @throws NotFoundException wird geworfen wenn MusicFile nicht vorhnden 
	 */
	public String downloadMusicFile(final MuFi element) throws NotFoundException {
		return this.user.downloadMuFi(element);
	}
	
	
	/**
	 * gibt einen Liste mit download Pfaden zurueck von den Titeln, welche der User gekauft hat
	 * @return ArrayList<String> ist die Liste mit den zu downloadenden Titeln
	 */
	public List<String> downloadUserMusicFileList() {
		//ArrayList<String> resultPathList = new ArrayList<String>(100);
		return this.user.downloadMusicFileList();

	}
	
	
	/**
	 * gibt einen Liste mit Download Pfaden zurueck von den Titeln, welche der User in seiner Playliste hat
	 * @return ArrayList<String> ist die Liste mit den zu downloadenden Titeln
	 */	
	public List<String> downloadUserPlaylist() {
		//ArrayList<String> resultPathList = new ArrayList<String>(100);
		return this.user.downloadMusicFileList();
	}
	
		
	/**
	 * bezahlen des Einkaufwagens, veranlasst aktualisieren des Guthabens und der TitelListe 
	 * @param user ist das Profil des bezahlenden Users
	 */
	public boolean buyShoppingCart(final Profile user) {
		return cart.payCart(user);
	}
	
	/**
	 * fuegt ein bestimmtes MusicFile dem Warenkorb hinzu
	 * @param mid ist bezeichnet die MusikFile id
	 * @return true falls erfolgreich
	 */
	public boolean addFileToShoppingCart(int mid) {
		MuFi m = MuFi.getById(mid);
		if(user != null && user.getUserTitleList().contains(m)) {
			return false;
		} else {
			cart.addTitleToCart(mid);
			return true;
		}
	}
	
	
	public Set<MuFi> getCart() {
		return cart.showcart();
	}
	

	/**
	 * loescht ein bestimmtes MusicFile aus dem Warenkorb 
	 * @param mid ist bezeichnet die MusikFile id
	 * @return true falls erfolgreich
	 */
	public boolean removeFileFromShoppingCart(int mid) {
		cart.deleteTitleFromCart(mid);
		return true;
	}
	
	
	/**
	 * Aktiviert das aktuelle Profil
	 */	
	public void activateProfile() {
		if(this.user != null) {
			this.user.setUserStatus(true);
		}
	}
	
	
	/**
	 * zeigt die eigene Musiktitelliste an
	 */
	public void showOwnMusicFiles() {
		user.getUserTitleList();
	}
	
	
	/**
	 * aendert die Mail Adresse angegebenen Passwortes
	 * @param user ist das zu uebergebene Profil
	 * @param newUserEmail ist die neue Emailadresse
	 * @param checkUserEmail ist die Kontroll emailadresse
	 * @return boolean true falls erfolgreich 
	 * @throws NotEqualException wird geworfen wenn die checkUserEmail nicht mit der aktuellen Email uebereinstimmen 
	 */
	public boolean changeEmail(Profile user, String newUserEmail ,String checkUserEmail ) throws NotEqualException {
		user.changeUserEmail(newUserEmail, checkUserEmail);
		return true;
	}
	
	
	/**
	 * aendert den Usernamen 
 	 * @param newName neuer username  
 	 * @param user das zu uebergebene Profil
	 */
	public void changeUsername(Profile user, String newName) {
		user.setUserName(newName);
	}
	
	/**
	 * gibt das Profil das aktuellen Users zurueck
	 * @return das Profil des aktuellen Users
	 */
	public Profile getUser() {
		return user;
	}
	
	/**
	 * Validiert eine Email Adresse nach einem bestimmten Muster
	 * @param hex - zu pruefende Adresse
	 * @return true, wenn Muster zutrifft, anderenfalls false
	 */
	public boolean validateEmailAdress(final String hex) {
		return Email.validateEmailAdress(hex);
	}
	
	/**
	 * Validiert Nutzernamen nach besitmmten Regeln
	 * @param uname - der zu pruefende Name
	 * @return true, wenn Muster zutrifft, anderenfalls false
	 */
	public boolean validateUsername(String uname) {
		return true;
	}
}

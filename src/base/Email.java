/**
 * 
 */
package base;

import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Dennis, Rene, Benjamin, Fritz, Michael
 * @version 1.1
 * Description: Erstellt Emails die durch das System versendet werden, ist leider nicht implementiert, 
 * weil kein EMail Server vorhanden  
 */
public class Email {
	private String sender;
	private String reciever;
	private String betreff;
	private String text;
	private Date date;
	private int ownerId;
	private final String verificationCode;
	
	private final char[] CHARS = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
	
	/**
	 * Konstruktor für eine Mail ( In diesem fall eine Verifikationsmail an einen neuen user 
	 * @param profile ist das Profil welches aktiviert werden soll, deshalb wird die Mail 
	 * auch an die im Profil angegebene Adresse gesendet
	 */
	public Email(final Profile profile){
		profile.setVerificationCode(this.verificationCode);
		this.betreff ="Verification deines Accounts bei I_LOVE_MUSIC";
		this.text = "Sehr geehrter " + profile.getUserName() + "\tVielen Dank fuer Ihre Registrierung bei I_LOVE_MUSIC \n Bitte aktivieren Sie Ihren Account mit dem unten stehenden Code" + "\n Code: " + "WWW:I_LOVE_MUSIC/" + this.verificationCode;
		this.date = new Date();
		this.ownerId = profile.getUid();
		this.sender = "I_LOVE_MUSIC@fantastic.geek";
		this.verificationCode = genVerificationCode();
		this.reciever = profile.getUserEmail();
	}

	/**
	 * Getter für den Betreff 
 	 * @return Betreff ist der Grund fuer die Mail
	 */
	public String getBetreff() {
		return betreff;
	}

	/**
	 * Getter fur den Text 
	 * @return text  ist der Email Text 
	 */
	public String getText() {
		return text;
	}

	/**
	 *Getter für das Datum 
	 * @return  date ist das Datum an dem die Mail erstellt wurde  
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * Gibt die UID des Profiles an den die Mail geht zurück
	 * @return the ownerId = UID des gewaehlten Profils
	 */
	public int getOwnerId() {
		return ownerId;
	}

	/**
	 * Setter für den EMail Betreff
	 * @param betreff ist der Grund fuer die Mail
	 */
	public void setBetreff(final String betreff) {
		this.betreff = betreff;
	}

	/**
	 * Setter fuer den EMail text
	 * @param text ist der gewünschte Inhalt der Mail
	 */
	public void setText(final String text) {
		this.text = text;
	}

	/**
	 * Setter fuer das Datum 
	 * @param date ist das Datum, welches der Mail beigefuegt wird
	 */
	public void setDate(final Date date) {
		this.date = date;
	}

	/**
	 * Setter fuer die UID 
	 * @param ownerId ist die UID das gewuenschten Profiles
	 */
	public void setOwnerId(final int ownerId) {
		this.ownerId = ownerId;
	}
	
	/**
	 * generiert einen Verificationscode für Profilaktivierung
	 * @return String Code ist der 5 stellige Verifications Key aus Zahlen 
	 * und Buchstaben für die Aktivierung eines Profils
	 */	
	private String genVerificationCode() {		
		final StringBuilder varString = new StringBuilder();
		final Random random = new Random();
		
		//code-Laenge 5
		for (int i = 0; i < 5; i++) {
			final char character = CHARS[random.nextInt(CHARS.length)];
			varString.append(character);
		}
		return varString.toString();
	}
	
	/**
	 * sendet Verfifkationsmail 
	 * @param mail ist die zu versendende EMail
	 */
	public static void sendMail(final Email mail){
		// Sending an email...
	}

	/**
	 * Getter für den Empfaenger der Mail
	 * @return EMail Adresse des Empfaengers  
	 */
	public String getReciever() {
		return reciever;
	}

	/**
	 * Getter fuer den Absender der E-Mail
	 * @return Mailadresse des Absenders
	 */
	
	public String getSender() {
		return sender;
	}

	/**
	 * Setter für den Absender 
	 * @param sender ist der Urheber der E-Mail
	 */
	public void setSender(final String sender) {
		this.sender = sender;
	}
	
	/**
	 * Validiert eine Email nach einem vorgegebenen Muster
	 * @param hex : die zu prüfende Email-Adresse
	 * @return true, wenn das Muster zutrifft, ansonsten false
	 */
	public static boolean validateEmailAdress(final String hex) {
		Pattern pattern;
		Matcher matcher;
		final String EMAIL_PATTERN = 
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			  + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(hex);
		return matcher.matches();
	}
}

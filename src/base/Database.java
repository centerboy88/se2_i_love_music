package base;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import base.db.XMLCouponFile;
import base.db.XMLMusicFile;
import base.db.XMLProfileFile;

/**
 * Diese Klasse stellt die Verbindung zur Datenbank bereit.
 * In der Testumgebung wird diese Datenbank durch Listen
 * von Elementen simuliert, in denen die Daten gespeichert 
 * werden.
 * 
 * @author Dennis, Rene, Fritz, Benjamin, Michael 
 * @version 1.1  
 *
 */
public class Database {
	
	private static final Logger LOG = Logger.getLogger(Database.class.getName()); 

	private transient Set<MuFi> mufiList;	
	private transient Set<Profile> profileList;
	private transient Set<Coupon> couponList;
	
	/**
	 * Privater Klassenkonstruktor
	 */
	public Database() {
		this.mufiList = 	XMLMusicFile.read();
		this.profileList = 	XMLProfileFile.read();
		this.couponList = 	XMLCouponFile.read();
	}
	
	// ==============================================
	// MusicFile Operationen
	// ==============================================
	
	/**
	 * Gibt eine Kopie der entsprechenden Musikdatei mit der 
	 * gewünschten ID zurueck
	 * @param mid ID des Musikfiles
	 * @return MuFi, oder Null
	 * @throws CloneNotSupportedException 
	 */
	public MuFi getMuFi(final int mid) {
		this.mufiList = XMLMusicFile.read();
		for(MuFi m: mufiList) {
			if(mid == m.getId()) {
				try {
					return m.clone();
				} catch (CloneNotSupportedException e) {
					LOG.log(Level.WARNING, "Get MuFi: " + e);
				}
			}
		}
		return null;
	}
	
	/**
	 * Getter-Methode für MusicFiles
	 * @return Menge der MusicFiles in der Datenbank
	 * @throws CloneNotSupportedException 
	 */
	public Set<MuFi> getMuFis() {
		this.mufiList = XMLMusicFile.read();
		final Set<MuFi> tmp = new TreeSet<MuFi>();
		for(MuFi m: mufiList) {
			try {
				tmp.add(m.clone());
			} catch (CloneNotSupportedException e) {
				LOG.log(Level.WARNING, "MuFi konnte nicht kopiert werden");
			}
		}
		return tmp;
	}
	
	/**
	 * Updatet ein MuFi in der Datenbank
	 * @param file1 Das zu aktualisierende MusicFile
	 * @return true wenn das MusicFile aktualisiert wurde, sonst false
	 * @throws CloneNotSupportedException  
	 */
	public boolean setMuFi(final MuFi file1) {
		this.mufiList = XMLMusicFile.read();
		final int mid = file1.getId();
		for(MuFi file2: mufiList) {
			if(mid == file2.getId()) {
				mufiList.remove(file2);
				try {
					mufiList.add(file1.clone());
				} catch (CloneNotSupportedException e) {
					LOG.log(Level.WARNING, "MuFi konnte nicht kopiert werden");
				}
				XMLMusicFile.write(mufiList);
				return true;
			}
		}
		return false;
	}

	/**
	 * fuegt ein MusicFile in die Datenbank ein
	 * @param musicfile ist das hinzu zu fuegende MusicFile
	 * @return true falls erfolgreich ansonsten false 
	 * @throws CloneNotSupportedException 
	 */
	public boolean insertMuFi(final MuFi musicfile) {
		boolean stat = false;
		this.mufiList = XMLMusicFile.read();
		try {
			stat = mufiList.add(musicfile.clone());
		} catch (CloneNotSupportedException e) {
			LOG.log(Level.WARNING, "MuFi konnte nicht kopiert werden");
		}
		XMLMusicFile.write(mufiList);
		return stat;
	}
	
	/**
	 *  iteriert durch die Mufi's der Datenbank und erstellt inkrementell eine neue id
	 */
	public int createFileId() {
		this.mufiList = XMLMusicFile.read();
		int idMax = 0;
		for(MuFi m: mufiList){
			if(m.getId() > idMax){
				idMax = m.getId();
			}
		}	
		return idMax+1;
	}
	
	/**
	 * Loeschen eines MuFi aus der Datenbank 
	 * @param musicfile Das zu löschende MusicFile
	 * @return true, wenn MuFi geloescht, sonst false
	 */
	public boolean deleteMuFi(final MuFi musicfile) {
		final int mid = musicfile.getId();
		return deleteMuFi(mid);
	}
	/**
	 * Loeschen eines MuFi aus der Datenbank 
	 * @param id  des zu löschenden MusicFile's
	 * @return true, wenn MuFi geloescht, sonst false
	 */
	public boolean deleteMuFi(final int id) {
		boolean stat = false;
		this.mufiList = XMLMusicFile.read();
		for(MuFi m_temp: mufiList) {
			if(m_temp.getId() == id) {
				stat = mufiList.remove(m_temp);
				XMLMusicFile.write(mufiList);
				return stat;
			}
		}
		return false;
	}
	
	// ==============================================
	// Profile Operationen
	// ==============================================
	
	/**
	 * Gibt eine Kopie des Profils mit der übergebenen ID
	 * zurueck
	 * @param uid - ID des entsprechenden Profils
	 * @return Profil, oder Null
	 * @throws CloneNotSupportedException 
	 */
	public Profile getProfile(final int uid) {
		this.profileList = 	XMLProfileFile.read();
		for(Profile p: profileList) {
			if(uid == p.getUid()) {
				try {
					return (Profile) p.clone();
				} catch (CloneNotSupportedException e) {
					LOG.log(Level.WARNING, "Profile" + e);
				}
			}
		}
		return null;
	}
		
	/**
	 * gibt ein Profil zurueck
	 * @param uname ist das zurueck zu gebende Profil
	 * @return das gewuenschte Profil ansonsten null
	 */
	public Profile getProfile(final String uname) {
		this.profileList = 	XMLProfileFile.read();
		for(Profile p: profileList) {
			if(uname.equals(p.getUserName())) {
				try {
					return (Profile) p.clone();
				} catch (CloneNotSupportedException e) {
					LOG.log(Level.WARNING, "Get Profile: " + e);
				}
			}
		}
		return null;
	}
	
	/**
	 * Getter-Methode für Profile
	 * @return Menge der Profile in der Datenbank
	 * @throws CloneNotSupportedException 
	 */
	public Set<Profile> getProfiles() {
		this.profileList = 	XMLProfileFile.read();
		final Set<Profile> tmp = new TreeSet<Profile>();
		for(Profile p: profileList) {
			try {
				tmp.add((Profile) p.clone());
			} catch (CloneNotSupportedException e) {
				LOG.log(Level.WARNING, "Get Profile: " + e);
			}
		}
		return tmp;
	}
	
	/**
	 * Updatet ein Profil in der Datenbank
	 * @param profile1 Das zu aktualisierende Profil
	 * @return true wenn das Profil aktualisiert wurde, sonst false
	 * @throws CloneNotSupportedException 
	 */
	public boolean setProfile(final Profile profile1) {
		this.profileList = 	XMLProfileFile.read();
		int uid = profile1.getUid();
		for(Profile profil2: profileList) {
			if(uid == profil2.getUid()) {
				profileList.remove(profil2);
				try {
					profileList.add((Profile) profile1.clone());
				} catch (CloneNotSupportedException e) {
					LOG.log(Level.WARNING, "Set Profile: " + e);
				}
				XMLProfileFile.write(profileList);
				return true;
			}
		}
		return false;
	}
	

	
	/**
	 * fuegt ein Profil in die Datenbank ein 
	 * @param profile ist das Profil das eingefuegt werden soll
	 * @return true falls erfolgreich ansonsten false
	 * @throws CloneNotSupportedException 
	 */
	public boolean insertProfile(final Profile profile) {
		boolean stat = false;
		this.profileList = 	XMLProfileFile.read();
		try {
			stat = profileList.add((Profile) profile.clone());
		} catch (CloneNotSupportedException e) {
			LOG.log(Level.WARNING, "Insert Profile: " + e);
		}
		XMLProfileFile.write(profileList);
		return stat;
	}
	/**
	 * itteriert durch die Datenbank profile und erstellt neue Id 
	 * 
	 * @return neue Id
	 */
	public int createUid() {
		this.profileList = 	XMLProfileFile.read();
		int idMax = 0;
		for(Profile p: profileList){
			if(p.getUid() > idMax){
				idMax = p.getUid();
			}
		}
		return idMax+1;
	}

	public boolean deleteProfile(final Profile p) {
		return deleteProfile(p.getUid());
	}
	/**
	 * loescht ein Profil aus der Datenbank
	 * @param uid ist die UID des zu loeschenden Profil's
	 * @return true falls erfolgreich ansonsten false
	 */
	public boolean deleteProfile(final int uid) {
		boolean stat = false;
		this.profileList = 	XMLProfileFile.read();
		for(Profile p_temp: profileList) {
			if(p_temp.getUid() == uid) {
				stat = profileList.remove(p_temp);
				XMLProfileFile.write(profileList);
				return stat;
			}
		}
		return false;
	}
	/**
	 * ueberprueft ob ein User in der Datenabnk vorhanden ist
	 * @param user ist das zu ueberpruefende Profil
	 * @return true falls erfolgreich aansonsten false
	 */
	public boolean userNameExist(final String user) {
		this.profileList = 	XMLProfileFile.read();
		for(Profile p: profileList) {
			if(p.getUserName().equals(user)) {
				return true;
			}
		}
		return false;
	}
	
	// ==============================================
	// Coupon Operationen
	// ==============================================	
	
	/**
	 * gibt eine Liste aller Coupon aus der Datenbank zurueck
	 * @param code ist der Code des zurueck zugebenden Coupons
	 * @return den Liste aller Coupons in der Datenbank oder null
	 */
	public Coupon getCoupon(final String code){
		this.couponList = XMLCouponFile.read();
		for(Coupon c: couponList) {
			if(code.equals(c.getCode())) {
				try {
					return c.clone();
				} catch (CloneNotSupportedException e) {
					LOG.log(Level.WARNING, "Get Coupon: " + e);
				}
			}
		}
		return null;
	}
	
	/**
	 *  erstellt ein Coupon Liste aus der Datenbabnk
	 * @return tmp -> Set<Coupon>
	 */
	public Set<Coupon> getCoupons() {
		this.couponList = XMLCouponFile.read();
		final Set<Coupon> tmp = new TreeSet<Coupon>();
		for(base.Coupon c: couponList){
			try {
				tmp.add(c.clone());
			} catch (CloneNotSupportedException e) {
				LOG.log(Level.WARNING, "Get Coupon" + e);
			}
		}
		return tmp;
	}	
	/**
	 * loescht einen Coupon aus der Datenbank
	 * @param coupon ist der zuloeschende Coupon
	 * @return true falls erfolgreich ansonsten false
	 */
	public boolean deleteCoupon(final Coupon coupon){
		return deleteCoupon(coupon.getCode());
	}
	
	/**
	 * loescht einen Coupon aus der Datenbank
	 * @param code der Code des zuloeschenden Coupons
	 * @return true falls erfolgreich
	 */
	public boolean deleteCoupon(final String code){
		boolean stat = false;
		this.couponList = XMLCouponFile.read();
		for(Coupon c: couponList){
			if(c.getCode().equals(code)){
				stat = couponList.remove(c);
				XMLCouponFile.write(couponList);
				return stat;
			}
		}
		return false;
	}	
	
	public boolean insertCoupon(final Coupon coupon){
		boolean stat = false;
		this.couponList = XMLCouponFile.read();
		try {
			stat = couponList.add(coupon.clone());
		} catch (CloneNotSupportedException e) {
			LOG.log(Level.WARNING, "Insert Coupon:" + e);
		}
		XMLCouponFile.write(couponList);
		return stat;
	}
	
	/**
	 * fuegt einen Coupon in die Datenabank ein 
	 * @param coupon ist der Code des hin zu zufuegenden Coupons 
	 * @return true falls erfolgreich ansonsten false 
	 */
	public boolean setCoupon(final Coupon coupon){
		this.couponList = XMLCouponFile.read();
		final String couponCode = coupon.getCode();
		for(Coupon c2: couponList) {
			if(couponCode.equals(c2.getCode())) {
				couponList.remove(coupon);
				try {
					couponList.add(coupon.clone());
				} catch (CloneNotSupportedException e) {
					LOG.log(Level.WARNING, "Set Coupon");
				}
				XMLCouponFile.write(couponList);
				return true;
			}
		}
		return false;
	}
	

	/**
	 * prueft ob ein Coupon in der Datenbank existiert 
	 * @param code ist der Code des zu ueberpruefenden Coupons
	 * @return true falls erfolgreich ansonsen false
	 */
	public boolean couponExists(final String code){
		this.couponList = XMLCouponFile.read();
		for(Coupon c1: couponList){
			if(c1.getCode().equals(code)){
				return true;
			}
		}		
		return false;
	}

	/**
	 * gibt alle Listen der Datenbank aus  
	 */
	public void print() {
		this.mufiList = XMLMusicFile.read();
		this.profileList = XMLProfileFile.read();
		this.couponList = XMLCouponFile.read();
		
		LOG.log(Level.INFO, "DB-View: \nProfiles:{");
		for(Profile p : profileList) {
			LOG.log(Level.INFO, p.toString() + ", ");
		}
		LOG.log(Level.INFO, "}, \nMuFi:{");
		for(MuFi m : mufiList) {
			LOG.log(Level.INFO, m.toString() + ", ");
		}
		LOG.log(Level.INFO, "}, \nCoupons:{");
		for(Coupon c : couponList) {
			LOG.log(Level.INFO, c.toString() + ", ");
		}
		LOG.log(Level.INFO, "}");
	}
	
	
}


import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import base.MusicPlattform;


public class MPAdmin {
	
	private transient MusicPlattform plattform;

	private transient JFrame adminbereich;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPAdmin window = new MPAdmin(new MusicPlattform());
					window.adminbereich.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void show(final MusicPlattform mp) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPAdmin window = new MPAdmin(mp);
					window.adminbereich.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MPAdmin(MusicPlattform mp) {
		this.plattform = mp;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		adminbereich = new JFrame();
		adminbereich.setTitle("Adminbereich");
		adminbereich.setBounds(100, 100, 300, 200);
		//Adminbereich.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		adminbereich.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 300, 167);
		adminbereich.getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton btnGutscheinverwaltung = new JButton("Gutscheinverwaltung");
		btnGutscheinverwaltung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MPGutscheinverwaltung frameGutscheinverwaltung = new MPGutscheinverwaltung();
				frameGutscheinverwaltung.show();
			}
		});
		btnGutscheinverwaltung.setBounds(52, 12, 208, 25);
		panel.add(btnGutscheinverwaltung);
		
		JButton btnMusikverwaltung = new JButton("Musikverwaltung");
		btnMusikverwaltung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MPAdminMusic frameAdminMusic = new MPAdminMusic(plattform);
				frameAdminMusic.show(plattform);
			}
		});
		btnMusikverwaltung.setBounds(52, 57, 208, 25);
		panel.add(btnMusikverwaltung);
		
		JButton btnBenutzerverwaltung = new JButton("Benutzerverwaltung");
		btnBenutzerverwaltung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MPBenutzerVerwaltung frameBenutzerVerwaltung = new MPBenutzerVerwaltung(plattform);
				frameBenutzerVerwaltung.show(plattform);				
			}
		});
		btnBenutzerverwaltung.setBounds(52, 108, 208, 25);
		panel.add(btnBenutzerverwaltung);
	}
}


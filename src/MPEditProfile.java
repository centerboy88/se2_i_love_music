import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import base.MusicPlattform;
public class MPEditProfile {

	private JFrame frmProfilBearbeiten;
	private JTextField txtName;
	private JTextField txtEmail;
	private MusicPlattform plattform;
	private JPasswordField pwPasswort;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPEditProfile window = new MPEditProfile(new MusicPlattform());
					window.frmProfilBearbeiten.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	
		
	public MPEditProfile(MusicPlattform mp) {
		this.plattform = mp;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmProfilBearbeiten = new JFrame();
		frmProfilBearbeiten.setTitle("Profil bearbeiten");
		frmProfilBearbeiten.setBounds(100, 100, 300, 250);
		frmProfilBearbeiten.getContentPane().setLayout(null);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(12, 12, 70, 15);
		frmProfilBearbeiten.getContentPane().add(lblName);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(12, 39, 70, 15);
		frmProfilBearbeiten.getContentPane().add(lblEmail);
		
		JLabel lblPasswort = new JLabel("Passwort:");
		lblPasswort.setBounds(12, 66, 138, 15);
		frmProfilBearbeiten.getContentPane().add(lblPasswort);
		
		txtName = new JTextField();
		txtName.setBounds(120, 10, 114, 19);
		txtName.setText(plattform.getUser().getUserName());
		frmProfilBearbeiten.getContentPane().add(txtName);
		txtName.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(120, 37, 114, 19);
		txtEmail.setText(plattform.getUser().getUserEmail());
		frmProfilBearbeiten.getContentPane().add(txtEmail);
		txtEmail.setColumns(10);
		
		pwPasswort = new JPasswordField();
		pwPasswort.setBounds(120, 64, 114, 19);
		frmProfilBearbeiten.getContentPane().add(pwPasswort);
		
		// MUSS NOCH AUF PROFIL ANGEPASST WERDEN
		
		JButton btnSpeichern = new JButton("Speichern");
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			String newName = txtName.getText();
			String newMail = txtEmail.getText();
			String password = new String(pwPasswort.getPassword());
			String oldMail = plattform.getUser().getUserEmail();
			String oldName = plattform.getUser().getUserName();
			if(plattform.getUser().checkPassword(password)){	
				if(!(newName.equals(oldName))){
					if(plattform.validateUsername(newName)){
						plattform.getUser().setUserName(newName);
						JOptionPane.showMessageDialog(frmProfilBearbeiten, "Der Name wurde geaendert", "OK", JOptionPane.INFORMATION_MESSAGE);

					}
					else {
						JOptionPane.showMessageDialog(frmProfilBearbeiten, "Der Name ist ungueltig", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
					}	
				}
				if(!(newMail.equals(oldMail))){
					if(plattform.validateEmailAdress(newMail)){
						plattform.getUser().setUserEmail(newMail);
						JOptionPane.showMessageDialog(frmProfilBearbeiten, "Ihre Emailadresse wurde geaendert", "OK", JOptionPane.INFORMATION_MESSAGE);

					}
					else{
						JOptionPane.showMessageDialog(frmProfilBearbeiten, "Die Emailadresse ist ungueltig", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
			else JOptionPane.showMessageDialog(frmProfilBearbeiten, "Das eingegebene Passwort ist falsch", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
			}
			
			
		});
		btnSpeichern.setBounds(150, 169, 117, 25);
		frmProfilBearbeiten.getContentPane().add(btnSpeichern);
		
		JButton btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmProfilBearbeiten.dispose();
			}
		});
		btnAbbrechen.setBounds(12, 169, 117, 25);
		frmProfilBearbeiten.getContentPane().add(btnAbbrechen);
		
		JButton btnPasswortAendern = new JButton("Passwort aendern");
		btnPasswortAendern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MPEditPassword editPassword = new MPEditPassword(plattform);
				editPassword.show(plattform);
			}
		});
		btnPasswortAendern.setBounds(51, 132, 183, 25);
		frmProfilBearbeiten.getContentPane().add(btnPasswortAendern);
				
	}
	public void show(final MusicPlattform mp) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPEditProfile window = new MPEditProfile(mp);
					window.frmProfilBearbeiten.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import base.MusicPlattform;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class MPEditPassword {

	private JFrame frmPasswortBearbeiten;
	private JPasswordField pwOldPassword;
	private JPasswordField pwNewPassword;
	private JPasswordField pwNewPassword2;
	private MusicPlattform plattform;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPEditPassword window = new MPEditPassword(new MusicPlattform());
					window.frmPasswortBearbeiten.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MPEditPassword(MusicPlattform mp) {
		this.plattform = mp;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPasswortBearbeiten = new JFrame();
		frmPasswortBearbeiten.setTitle("Passwort bearbeiten");
		frmPasswortBearbeiten.setBounds(100, 100, 300, 300);
		frmPasswortBearbeiten.getContentPane().setLayout(null);
		
		JLabel lblAltesPasswort = new JLabel("Altes Passwort:");
		lblAltesPasswort.setBounds(12, 12, 125, 15);
		frmPasswortBearbeiten.getContentPane().add(lblAltesPasswort);
		
		JLabel lblNeuesPasswort = new JLabel("Neues Passwort:");
		lblNeuesPasswort.setBounds(12, 39, 125, 15);
		frmPasswortBearbeiten.getContentPane().add(lblNeuesPasswort);
		
		JLabel lblNeuesPasswort_1 = new JLabel("Neues Passwort:");
		lblNeuesPasswort_1.setBounds(12, 66, 125, 15);
		frmPasswortBearbeiten.getContentPane().add(lblNeuesPasswort_1);
		
		pwOldPassword = new JPasswordField();
		pwOldPassword.setBounds(155, 10, 109, 19);
		frmPasswortBearbeiten.getContentPane().add(pwOldPassword);
		
		pwNewPassword = new JPasswordField();
		pwNewPassword.setBounds(155, 37, 109, 19);
		frmPasswortBearbeiten.getContentPane().add(pwNewPassword);
		
		pwNewPassword2 = new JPasswordField();
		pwNewPassword2.setBounds(155, 64, 109, 19);
		frmPasswortBearbeiten.getContentPane().add(pwNewPassword2);
		
		JButton btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmPasswortBearbeiten.dispose();
			}
		});
		btnAbbrechen.setBounds(12, 109, 117, 25);
		frmPasswortBearbeiten.getContentPane().add(btnAbbrechen);
		
		JButton btnSpeichern = new JButton("Speichern");
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String oldPw = new String(pwOldPassword.getPassword());
				String newPw = new String(pwNewPassword.getPassword());
				String newPw2 = new String(pwNewPassword2.getPassword());
				
				System.out.println("Altes: " + oldPw);
				if(plattform.getUser().checkPassword(oldPw)){
					if(newPw.equals(newPw2)){
							plattform.getUser().changePassword(newPw);
					}
					else {
						JOptionPane.showMessageDialog(frmPasswortBearbeiten, "Die Neuen Passwoerter stimmen nicht überein", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
					}
				}
				else {
					JOptionPane.showMessageDialog(frmPasswortBearbeiten, "Das eingegebene Passwort ist falsch", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
				}
				JOptionPane.showMessageDialog(frmPasswortBearbeiten, "Ihr Passwort wurde erfolgreich geaendert", "OK", JOptionPane.INFORMATION_MESSAGE);
				frmPasswortBearbeiten.dispose();
				
				
			}
		});
		btnSpeichern.setBounds(155, 109, 117, 25);
		frmPasswortBearbeiten.getContentPane().add(btnSpeichern);
	}
	public void show(final MusicPlattform mp) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPEditPassword window = new MPEditPassword(mp);
					window.frmPasswortBearbeiten.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

import java.util.List;

import javax.swing.table.AbstractTableModel;

import base.Database;
import base.MuFi;
import base.MusicPlattform;
import base.ShoppingCart;




@SuppressWarnings("serial")
class MPTableModelMusic extends AbstractTableModel {
    private String[] columnNames; 
    private int[] columnsEditable;
    private Object[][] data;
    Database db = new Database();
    //private MusicPlattform plattform = new MusicPlattform();
    
    public MPTableModelMusic(MusicPlattform plattform, String[] colNames, int[] colsEdit, Boolean onlyOwn) {
    	super();
    	columnNames = colNames;
    	columnsEditable = colsEdit;
    	if(onlyOwn == false){
        	data = MusicPlattform.setObjectArrayConverter(db.getMuFis());   	
        	}
        	else{
        		data = MusicPlattform.listObjectArrayConverter(plattform.getUser().getUserTitleList());
        	}
    }
    
    public MPTableModelMusic(MusicPlattform plattform, String[] colNames, int[] colsEdit) {
    	super();
    	columnNames = colNames;
    	columnsEditable = colsEdit;
    	if(plattform.isCartEmpty()) {
    		data = new Object[0][0];
    	}
    	else {
    		data = MusicPlattform.setObjectArrayConverter(plattform.getCart());
    	}
    }
    
    public MPTableModelMusic(MusicPlattform plattform, String[] colNames, int[] colsEdit, String search, boolean onlyOwn) {
    	super();
    	columnNames = colNames;
    	columnsEditable = colsEdit;
    	if(onlyOwn == false){
    	data = MusicPlattform.listObjectArrayConverter(ShoppingCart.search(search));   	
    	}
    	else{
    		data = MusicPlattform.listObjectArrayConverter(plattform.getUser().getUserTitleList());
    	}
    	}
    
    public MPTableModelMusic(List<MuFi> newData) {
    	super();
    	newData.toArray(data);
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.length;
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
        return data[row][col];
    }

    public Class<?> getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    /*
     * Don't need to implement this method unless your table's
     * editable.
     */
    public boolean isCellEditable(int row, int col) {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
    	for(int i = 0; i < columnsEditable.length; i++) {
    	   	if(columnsEditable[i] == col) {
    	   		return true;
    	   	}
    	}
    	return false;
    }

    /*
     * Don't need to implement this method unless your table's
     * data can change.
     */
    public void setValueAt(Object value, int row, int col) {
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }

}

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Timer;

import base.MusicPlattform;
import base.Role;
import base.exceptions.IncorrectPasswordException;
import base.exceptions.NoProfileFoundException;



public class MPMainFrame {
	
	private final static MusicPlattform PLATTFORM = new MusicPlattform();
	private static final Logger LOG = Logger.getLogger(MPMainFrame.class.getName());
	
	// Hauptfenster
	private JFrame mainWindow;
	
	// Panel
	private JPanel searchPanel;
	private JScrollPane scrollPane;
	private JPanel menuPanel;
	private JLabel lblName;
	private JLabel lblPasswort;
	private JLabel lblLoggedInAs;
	
	// Textfelder
	private JTextField txtUsername;
	private JTextField txtSearchbar;
	private JCheckBox chkOnlyOwn;
	
	// Passwortfeld
	private JPasswordField txtPassword;
	
	// Tabellenansicht
	private JTable table;
	
	// Buttons
	private JButton btnLogin;
	private JButton btnLogout;
	private JButton btnaddToCart;
	private JButton btnRegistrieren;
	private JButton btnSearchButton;
	private JButton btnWarenkorbAnzeigen;
	private JButton btnAdminAnsicht;
	private JButton btnGutscheinEinloesen;
	private JButton btnTitelHerunterladen;
	
	//Stuff
	private String[] columnNames = {"", "Titel", "Interpret", "Album", "Genre", "Preis", "Kaufen" };
	private int[] columnsEditable = {6}; //nur kaufen-Spalte
	private JButton btnProfilBearbeiten;

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		Timer t = new Timer(300, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				PLATTFORM.updateData();
				//LOG.info("Plattform aktualisiert");
			}
		});
		t.start();
		
		//
		// = = Hauptansicht = =
		//
		mainWindow = new JFrame();
		mainWindow.setResizable(false);
		mainWindow.setTitle("iLoveMusic");
		mainWindow.setBounds(100, 100, 800, 600);
		mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainWindow.getContentPane().setLayout(null);
	
		//
		// = = Menübereich = =
		//
		menuPanel = new JPanel();
		menuPanel.setBounds(12, 12, 774, 139);
		mainWindow.getContentPane().add(menuPanel);
		menuPanel.setLayout(null);
		
		//
		// = = Suchbereich = =
		//
		searchPanel = new JPanel();
		searchPanel.setBounds(12, 163, 774, 392);
		mainWindow.getContentPane().add(searchPanel);
		searchPanel.setLayout(null);
		searchPanel.add(new JScrollPane());
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 762, 322);
		searchPanel.add(scrollPane);
		
		//
		// = = Tabelle mit Titeln = =
		//
		table = new JTable(new MPTableModelMusic(PLATTFORM, columnNames, columnsEditable, false));
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(0).setWidth(0);
		scrollPane.setViewportView(table);
		
		//
		// = = Buttons = =
		//
		
		btnaddToCart = new JButton("In den Warenkorb");
		btnaddToCart.setBounds(544, 351, 181, 27);
		searchPanel.add(btnaddToCart);
		
		btnLogin = new JButton("Login");
		btnLogin.setBounds(634, 72, 122, 27);
		btnLogin.setToolTipText("Anmelden");
		menuPanel.add(btnLogin);
		
		btnSearchButton = new JButton("iLoveMusic Suche");
		btnSearchButton.setBounds(284, 33, 193, 27);
		menuPanel.add(btnSearchButton);
		
		btnLogout = new JButton("Logout");
		btnLogout.setBounds(639, 1, 117, 25);
		btnLogout.setVisible(false);
		menuPanel.add(btnLogout);
		
		btnRegistrieren = new JButton("Registrieren");
		btnRegistrieren.setBounds(634, 102, 122, 25);
		menuPanel.add(btnRegistrieren);
	
		btnWarenkorbAnzeigen = new JButton("Warenkorb anzeigen");
		btnWarenkorbAnzeigen.setBounds(12, 0, 183, 41);
		menuPanel.add(btnWarenkorbAnzeigen);
	
		btnAdminAnsicht = new JButton("Admin-Ansicht");
		btnAdminAnsicht.setBounds(12, 53, 183, 41);
		btnAdminAnsicht.setVisible(false);
		menuPanel.add(btnAdminAnsicht);
		
		btnGutscheinEinloesen = new JButton("Gutschein einloesen");
		btnGutscheinEinloesen.setBounds(12, 53, 183, 41);
		btnGutscheinEinloesen.setVisible(false);
		menuPanel.add(btnGutscheinEinloesen);
			
		lblLoggedInAs = new JLabel();
		lblLoggedInAs.setBounds(12, 346, 339, 32);
		searchPanel.add(lblLoggedInAs);
		
		btnTitelHerunterladen = new JButton("Titel herunterladen");
		btnTitelHerunterladen.setBounds(581, 346, 181, 27);
		btnTitelHerunterladen.setVisible(false);
		searchPanel.add(btnTitelHerunterladen);
		
		//
		// = = Text- und Passwortfelder = =
		//
		txtUsername = new JTextField();
		txtUsername.setToolTipText("Bitte Benutzernamen eingeben.");
		txtUsername.setBounds(634, 0, 122, 27);
		menuPanel.add(txtUsername);
		txtUsername.setColumns(10);
		
	
		txtPassword = new JPasswordField();
		txtPassword.setToolTipText("Bitte geben Sie Ihr Passwort ein.");
		txtPassword.setEchoChar('?');
		txtPassword.setBounds(634, 33, 122, 27);
		menuPanel.add(txtPassword);
		txtPassword.setColumns(10);
		
		txtSearchbar = new JTextField();
		txtSearchbar.setBounds(246, 0, 256, 27);
		menuPanel.add(txtSearchbar);
		txtSearchbar.setColumns(10);
		
		chkOnlyOwn = new JCheckBox("Eigene Titel");
		chkOnlyOwn.setBounds(317, 74, 128, 23);
		chkOnlyOwn.setVisible(false);
		menuPanel.add(chkOnlyOwn);
		
		btnProfilBearbeiten = new JButton("Profil bearbeiten");
		btnProfilBearbeiten.setBounds(602, 34, 154, 25);
		btnProfilBearbeiten.setVisible(false);
		menuPanel.add(btnProfilBearbeiten);
		
		lblName = new JLabel("Name:");
		lblName.setBounds(530, 6, 70, 15);
		lblName.setVisible(true);
		menuPanel.add(lblName);
		
		lblPasswort = new JLabel("Passwort:");
		lblPasswort.setBounds(530, 39, 85, 15);
		lblPasswort.setVisible(true);
		menuPanel.add(lblPasswort);
		
			
		//
		// = = ActionListener = =
		//
		
		/*
		 * 
		 */
		
		btnaddToCart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < table.getModel().getRowCount(); i++) {
					if ((boolean) table.getModel().getValueAt(i, 6)) {
						PLATTFORM.addFileToShoppingCart((int) table.getModel().getValueAt(i, 0));
						table.getModel().setValueAt(false, i, 6);
					}
				}
			}
		});
		
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String uname = txtUsername.getText();
				char[] pw = txtPassword.getPassword();
				if(uname.equals("") || (pw.length == 0)) {
					LOG.info("Unvollständige Eingabe.");
					JOptionPane.showMessageDialog(mainWindow, "Keine vollstaendige Eingabe", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
				}
				else {
					try {
						PLATTFORM.login(uname, pw);
						txtUsername.setVisible(false);
						txtPassword.setVisible(false);
						chkOnlyOwn.setVisible(true);
						btnLogin.setVisible(false);
						btnRegistrieren.setVisible(false);
						lblName.setVisible(false);
						lblPasswort.setVisible(false);
						btnLogout.setVisible(true);
						btnProfilBearbeiten.setVisible(true);
						if(PLATTFORM.getUser().getUserStatus())
						btnProfilBearbeiten.setVisible(true);
						JOptionPane.showMessageDialog(mainWindow, "Herzlich Willkommen bei ILoveMusic", "Erfolgreich eingeloggt", JOptionPane.INFORMATION_MESSAGE);

						// Textfelder leeren
						txtUsername.setText("");
						txtPassword.setText("");
						lblLoggedInAs.setText("Angemeldet als: " + PLATTFORM.getUser().getUserName());
						if (PLATTFORM.getUser().getUserRole() == Role.admin) {
							btnAdminAnsicht.setVisible(true);
						}
						else {
							btnGutscheinEinloesen.setVisible(true);
						}
					} catch(NoProfileFoundException e) {
						LOG.log(Level.WARNING, "Exception:" + e.getMessage());
						JOptionPane.showMessageDialog(mainWindow, "Falscher Benutzername!", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
					} catch(IncorrectPasswordException e) {
						LOG.log(Level.WARNING, "Exception:" + e.getMessage());
						JOptionPane.showMessageDialog(mainWindow, "Passwort inkorrekt!", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
					}
					
				}
			}
		});
		
		btnSearchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String searchString = txtSearchbar.getText();
				table.setModel(new MPTableModelMusic(PLATTFORM, columnNames, columnsEditable, searchString, chkOnlyOwn.isSelected()));
        		table.getColumnModel().getColumn(0).setMinWidth(0);
        		table.getColumnModel().getColumn(0).setMaxWidth(0);
        		table.getColumnModel().getColumn(0).setWidth(0);
        		if(chkOnlyOwn.isSelected()) {
        			btnaddToCart.setVisible(false);
        			btnTitelHerunterladen.setVisible(true);
        		} else {
        			btnaddToCart.setVisible(true);
        			btnTitelHerunterladen.setVisible(false);
        		}
			}
		});
		
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PLATTFORM.logout();
				txtUsername.setVisible(true);
				txtPassword.setVisible(true);
				btnLogin.setVisible(true);
				btnRegistrieren.setVisible(true);
				btnLogout.setVisible(false);
				lblLoggedInAs.setText("");
				btnAdminAnsicht.setVisible(false);
				btnGutscheinEinloesen.setVisible(false);
				PLATTFORM.clearCart();
				btnProfilBearbeiten.setVisible(false);
				chkOnlyOwn.setSelected(false);
				chkOnlyOwn.setVisible(false);
			}
		});
		
		btnRegistrieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MPRegister frameRegister = new MPRegister(PLATTFORM);
				frameRegister.show(PLATTFORM);
			}
			
		});
		
		btnWarenkorbAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MPWarenkorbAnzeigen frameWarenkorb = new MPWarenkorbAnzeigen(PLATTFORM);
				frameWarenkorb.show(PLATTFORM);
			}
		});
		
		btnAdminAnsicht.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MPAdmin frameAdmin = new MPAdmin(PLATTFORM);
				frameAdmin.show(PLATTFORM);
			}
		});
		
		btnProfilBearbeiten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MPEditProfile frameEditProfile = new MPEditProfile(PLATTFORM);
				frameEditProfile.show(PLATTFORM);
			}
		});
		
		btnGutscheinEinloesen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MPGutscheinEinloesen frameGutschein = new MPGutscheinEinloesen(PLATTFORM);
				frameGutschein.show(PLATTFORM);
			}
		});
		
		btnTitelHerunterladen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(mainWindow, "Ihre Titel wären jetzt heruntergeladen worden!", "Info", JOptionPane.INFORMATION_MESSAGE);
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MPMainFrame() {
		initialize();
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPMainFrame window = new MPMainFrame();
					window.mainWindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

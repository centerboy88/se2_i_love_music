
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import base.Database;
import base.MuFi;
import base.MusicPlattform;


public class MPAdminMusic {
	
	private MusicPlattform plattform;

	private JFrame frmMusikVerwaltung;
	private JTextField searchField;
	private JTable table;
	private Database db = new Database();
	private MuFi m;
	
	private String[] columnNames = {"ID", "Titel", "Interpret", "Album", "Genre", "Preis", "Loeschen" };
	private int[] columnsEditable = {1,2,3,4,5,6};
	private String searchString = "";
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPAdminMusic window = new MPAdminMusic(new MusicPlattform());
					window.frmMusikVerwaltung.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void show(final MusicPlattform mp) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPAdminMusic window = new MPAdminMusic(mp);
					window.frmMusikVerwaltung.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MPAdminMusic(MusicPlattform mp) {
		this.plattform = mp;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMusikVerwaltung = new JFrame();
		frmMusikVerwaltung.setTitle("Musikverwaltung");
		frmMusikVerwaltung.setBounds(100, 100, 600, 444);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
		JButton btnTitelHinzufuegen = new JButton("Titel hinzufuegen");
		btnTitelHinzufuegen.setBounds(12, 26, 159, 25);
		btnTitelHinzufuegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MPAddTrack frameAddTrack = new MPAddTrack(plattform);
				frameAddTrack.show(plattform);
			}
		});
		frmMusikVerwaltung.getContentPane().setLayout(null);
		frmMusikVerwaltung.getContentPane().add(btnTitelHinzufuegen);
		
		JButton btnTitelLschen = new JButton("Titel loeschen");
		btnTitelLschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = JOptionPane.showConfirmDialog(frmMusikVerwaltung," Wollen Sie die ausgewaehlten Titel wirklich loeschen?","Loeschen", JOptionPane.YES_NO_OPTION);
				if (i == 0){
				for (int row = 0; row < db.getMuFis().size(); row++) {
					if((boolean) table.getModel().getValueAt(row, 6)) {
						db.deleteMuFi((int) table.getModel().getValueAt(row, 0));
					}
				}
				JOptionPane.showMessageDialog(frmMusikVerwaltung, "Ihre Aenderungen wurden erfolgreich uebernommen.", "Erfolg", JOptionPane.INFORMATION_MESSAGE);
				table.setModel(new MPTableModelMusic(plattform, columnNames, columnsEditable, searchString, false));
				}
				
			}	
		});
		btnTitelLschen.setBounds(12, 63, 159, 25);
		frmMusikVerwaltung.getContentPane().add(btnTitelLschen);
		
		JButton btnTitelBearbeiten = new JButton("Tabellenaenderungen Speichern");
		btnTitelBearbeiten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int row = 0; row < table.getRowCount(); row++) {
					m = db.getMuFi((int) table.getModel().getValueAt(row, 0));
					m.setTitle((String) table.getModel().getValueAt(row, 1));
					m.setArtist((String) table.getModel().getValueAt(row, 2));
					m.setAlbum((String) table.getModel().getValueAt(row, 3));
					m.setGenre((String) table.getModel().getValueAt(row, 4));
					m.setPrice((float) table.getModel().getValueAt(row, 5));
					db.setMuFi(m);
				}
				JOptionPane.showMessageDialog(frmMusikVerwaltung, "Ihre Aenderungen wurden erfolgreich uebernommen.", "Erfolg", JOptionPane.INFORMATION_MESSAGE);
				table.setModel(new MPTableModelMusic(plattform, columnNames, columnsEditable, searchString, false));
			}
		});
		btnTitelBearbeiten.setBounds(118, 374, 306, 25);
		frmMusikVerwaltung.getContentPane().add(btnTitelBearbeiten);
		
		searchField = new JTextField();
		searchField.setBounds(250, 29, 205, 19);
		frmMusikVerwaltung.getContentPane().add(searchField);
		searchField.setColumns(10);
		
		JButton btnSuchen = new JButton("Suchen");
		btnSuchen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchString = searchField.getText();
				//List<MuFi> suchErgebniss = plattform.searchTitle(searchString);
				table.setModel(new MPTableModelMusic(plattform, columnNames, columnsEditable, searchString, false));
			}
		});
		btnSuchen.setBounds(293, 49, 117, 25);
		frmMusikVerwaltung.getContentPane().add(btnSuchen);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 100, 574, 262);
		frmMusikVerwaltung.getContentPane().add(scrollPane);
		
		table = new JTable(new MPTableModelMusic(plattform, columnNames, columnsEditable, false));
		scrollPane.setViewportView(table);
		
	}

	MPGutscheinverwaltung frameGutscheinverwaltung = new MPGutscheinverwaltung();
	//frameGutscheinverwaltung.show();
}

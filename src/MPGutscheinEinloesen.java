import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import base.Coupon;
import base.Database;
import base.MusicPlattform;

public class MPGutscheinEinloesen {

	private JFrame frame;
	private MusicPlattform plattform;
	private JTextField txtGutschein;
	private JLabel lblIhrGutschein;
	private JButton btnEinloesen;
	private JButton btnAbbrechen;
	private Database db;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPGutscheinEinloesen window = new MPGutscheinEinloesen(new MusicPlattform());
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	


	/**
	 * Create the application.
	 */
	public MPGutscheinEinloesen(MusicPlattform mp) {
		this.plattform = mp;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 275, 180);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		txtGutschein = new JTextField();
		txtGutschein.setBounds(45, 55, 174, 19);
		frame.getContentPane().add(txtGutschein);
		txtGutschein.setColumns(10);
		
		lblIhrGutschein = new JLabel("Ihr Gutschein:");
		lblIhrGutschein.setBounds(84, 26, 114, 15);
		frame.getContentPane().add(lblIhrGutschein);
		
		btnEinloesen = new JButton("Einloesen");
		btnEinloesen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String gutschein = txtGutschein.getText();
				Coupon c = Coupon.getByCode(gutschein);
				if(Coupon.exists(gutschein)){
					if(c.isValid()){
						plattform.useCouponCode(plattform.getUser(), gutschein);
						JOptionPane.showMessageDialog(frame, "Der Gutschein wurde efolgreich eingeloest", "Erfolg", JOptionPane.INFORMATION_MESSAGE);
						
					}
					else{
						JOptionPane.showMessageDialog(frame, "Der Gutschein wurde bereits verbraucht", "Fehler", JOptionPane.ERROR_MESSAGE);
						
					}
				}
				else {
					JOptionPane.showMessageDialog(frame, "Der Gutschein existiert nicht", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnEinloesen.setBounds(158, 99, 103, 25);
		frame.getContentPane().add(btnEinloesen);
		
		btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.setBounds(12, 99, 121, 25);
		frame.getContentPane().add(btnAbbrechen);
		
		
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
	}
	public void show(final MusicPlattform mp) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPGutscheinEinloesen window = new MPGutscheinEinloesen(mp);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

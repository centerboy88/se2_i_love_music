import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import base.Coupon;
import base.Database;


public class MPGutscheinErstellen {

	private JFrame frame;
	private JTextField txtWert;
	private JTextField txtAnzahl;
	Database db = new Database();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPGutscheinErstellen window = new MPGutscheinErstellen();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public void show(){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPGutscheinErstellen window = new MPGutscheinErstellen();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MPGutscheinErstellen() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 300, 250);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Gutschein erstellen");
		
		JLabel lblWert = new JLabel("Wert:");
		lblWert.setBounds(12, 27, 70, 15);
		frame.getContentPane().add(lblWert);
		
		JLabel lblAnzahl = new JLabel("Anzahl:");
		lblAnzahl.setBounds(12, 81, 70, 15);
		frame.getContentPane().add(lblAnzahl);
		
		txtWert = new JTextField();
		txtWert.setBounds(84, 25, 114, 19);
		frame.getContentPane().add(txtWert);
		txtWert.setColumns(10);
		
		txtAnzahl = new JTextField();
		txtAnzahl.setBounds(84, 79, 114, 19);
		frame.getContentPane().add(txtAnzahl);
		txtAnzahl.setColumns(10);
		
		
		
		JButton btnErstellen = new JButton("Erstellen");
		btnErstellen.addActionListener(new ActionListener() {
			
			/**
			 * erstellt eine beliebeige Anzahl von Gutscheinen im Wert von x 
			 */
			
			public void actionPerformed(ActionEvent e) {	
				if((txtWert.getText().length() == 0) || (txtAnzahl.getText().length() == 0)) {
					System.out.println("Unvollständige Eingabe.");
					JOptionPane.showMessageDialog(frame, "Wert , Anzahl  muss mindestens Eins sein.", "Eingabefehler", JOptionPane.ERROR_MESSAGE);
				}
				else{
					// do something
					float couponValue = Integer.parseInt(txtWert.getText());
					int couponAmount = Integer.parseInt(txtAnzahl.getText()); 
					int counter = 0;
					boolean check  = Coupon.generateCoupons(couponValue, couponAmount);
					if (check == true){
						 	System.out.println( counter + "er Gutsschein erstellt mit Wert");
						 	txtAnzahl.setText("");
						 	txtWert.setText("");	
						 	JOptionPane.showMessageDialog(frame,"Gutschein/-e erfolgreich erstellt","Gutscheinverwaltung", JOptionPane.INFORMATION_MESSAGE);
						 	//SwingUtilities.updateComponentTreeUI(frame);
					}
				}
			}
		});
		btnErstellen.setBounds(84, 120, 117, 25);
		frame.getContentPane().add(btnErstellen);
		
		JButton btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnAbbrechen.setBounds(84, 157, 117, 25);
		frame.getContentPane().add(btnAbbrechen);
	}
}

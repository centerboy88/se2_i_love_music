import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import base.Database;
import base.MusicPlattform;
import base.Profile;
import base.Role;


public class MPBenutzerVerwaltung {

	private JFrame frmBenutzerverwaltung;
	private JTable table;
	private MusicPlattform plattform;
	private Database db = new Database();
	private Profile p;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPBenutzerVerwaltung window = new MPBenutzerVerwaltung(new MusicPlattform());
					window.frmBenutzerverwaltung.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MPBenutzerVerwaltung(MusicPlattform mp) {
		this.plattform = mp;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
			
		frmBenutzerverwaltung = new JFrame();
		frmBenutzerverwaltung.setTitle("Benutzerverwaltung");
		frmBenutzerverwaltung.setBounds(100, 100, 526, 409);
		frmBenutzerverwaltung.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 492, 237);
		frmBenutzerverwaltung.getContentPane().add(scrollPane);
		
		table = new JTable(new MPTableModelBenutzer());
		scrollPane.setViewportView(table);
		
		JButton btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmBenutzerverwaltung.dispose();
			}
		});
		btnAbbrechen.setBounds(48, 280, 117, 25);
		frmBenutzerverwaltung.getContentPane().add(btnAbbrechen);
		
		JButton btnUebernehmen = new JButton("Uebernehmen");
		btnUebernehmen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {				
				for (int row = 0; row < db.getProfiles().size(); row++) {
					p = db.getProfile((int) table.getModel().getValueAt(row, 0));
					p.setUserName((String) table.getModel().getValueAt(row, 1));
					p.setUserCredit((float) table.getModel().getValueAt(row, 2));
					if((boolean) table.getModel().getValueAt(row, 3)) {
						p.setUserRole(Role.admin);
					}
					else if(p.getUserName() != (String) table.getModel().getValueAt(row, 1)) {
						p.setUserRole(Role.user);
					}
					db.setProfile(p);
				}
				JOptionPane.showMessageDialog(frmBenutzerverwaltung, "Ihre Aenderungen wurden erfolgreich uebernommen.", "Erfolg", JOptionPane.INFORMATION_MESSAGE);
				table.setModel(new MPTableModelBenutzer());
			}
		});
		btnUebernehmen.setBounds(197, 280, 162, 25);
		frmBenutzerverwaltung.getContentPane().add(btnUebernehmen);
		
		JButton btnLoeschen = new JButton("Loeschen");
		btnLoeschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				for (int i = 0; i < table.getModel().getRowCount(); i++) {
					if((boolean) table.getModel().getValueAt(i, 4) == true){
						db.deleteProfile((int) table.getModel().getValueAt(i,0));
					}
				}
				table.setModel(new MPTableModelBenutzer());
			}
		});
		btnLoeschen.setBounds(387, 280, 117, 25);
		frmBenutzerverwaltung.getContentPane().add(btnLoeschen);
	}

public void show(final MusicPlattform mp) {
	EventQueue.invokeLater(new Runnable() {
		public void run() {
			try {
				MPBenutzerVerwaltung window = new MPBenutzerVerwaltung(mp);
				window.frmBenutzerverwaltung.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	});
}
}


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import base.Coupon;
import base.Database;
import base.Profile;



/**
 * @author Dennis, Rene, Benjamin, Fritz, Michael
 * 
 */
public class TestCoupon {

	Coupon coupon;
	Database database = new Database();
	List<Coupon>coupList = new ArrayList<Coupon>();
	Profile profile;

	@Before
	public void setUp() {
		coupon = new Coupon(5f);
		profile = new Profile("testuser", "muster@gmail.com","password");
		profile.setUserCredit(5f);
		database.insertCoupon(coupon);
		database.insertProfile(profile);
	}
	
	@Test
	public void testCoupon() { 
	    assertEquals(true, coupon.isValid());
	}

	@Test
	public void testVoidCoupon() {
		assertEquals(5, profile.getUserCredit(), 0);
		Coupon.voidCoupon(profile, coupon.getCode());
		assertFalse(database.getCoupon(coupon.getCode()).isValid());
		assertEquals(10, database.getProfile(profile.getUid()).getUserCredit(), 0);
	}
	
	@Test
	public void couponExists() {
		assertFalse(database.insertCoupon(coupon));
	}
	
	@Test
	public void testGenerate(){
		assertEquals(coupon.getCode(),database.getCoupon(coupon.getCode()).getCode());
	}
	
	@After
	public void rollback(){
		database.deleteCoupon(coupon);
	}
	
}	
		


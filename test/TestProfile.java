import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import base.Database;
import base.MuFi;
import base.Profile;
import base.exceptions.NoProfileFoundException;
import base.exceptions.NotEqualException;
import base.exceptions.NotFoundException;

public class TestProfile {

	private transient Profile testProfile;	
	private final Database database = new Database();
	
	@Before
	public void setUp() throws Exception {
		this.testProfile = new Profile("username","username@mail.com","password");
		database.insertProfile(testProfile);
		
	}
	@After
	public void cleanUp() throws Exception {
		database.deleteProfile(testProfile);
	}
	/**
	 * Testet ob der erstellte Benutzer auch dem entspricht der in der datenbank abgelegt wurde
	 * @throws NoProfileFoundException
	 */
	@Test
	public void testSetName() throws NoProfileFoundException {
		testProfile.setUserName("testuser10");
		database.setProfile(testProfile);
		Profile new_x = Profile.getProfile("testuser10");
		assertEquals(testProfile.getUid(),new_x.getUid());
	}
	
	@Test
	public void testGetUserCredit(){
		assertEquals(testProfile.getUserCredit(),0,0);
	}
	@Test
	public void testGetUserName(){
		assertEquals(testProfile.getUserName(),"username");
	}
	@Test
	public void testGetUserStatus(){
		assertEquals(testProfile.getUserStatus(),false);
	}
	@Test
	public void testGetUserMail(){
		assertEquals(testProfile.getUserEmail(),"username@mail.com");
	}

	@Test
	public void testTitleList(){
		MuFi e = new MuFi("title","artist","album","genre",0);
		assertNotNull(testProfile.getUserTitleList());
		assertTrue(testProfile.addTitelEndToTitleList(e));
		assertTrue(testProfile.getUserTitleList().contains(e));
		assertTrue(testProfile.removeTitleFromTitleList(testProfile.getUserTitleList(), e));
		assertTrue(testProfile.addTitelOnIndexToTitleList(e,testProfile.getUserTitleList(),0));
		assertTrue(testProfile.clearTitleList());
	}
	@Test 
	public void testPlaylist(){
		MuFi e = new MuFi("title","artist","album","genre",0);
		assertNotNull(testProfile.getUserPlaylist());
		assertTrue(testProfile.addTitelEndToPlayList(e));
		assertTrue(testProfile.removeTitleFromPlayList(testProfile.getUserPlaylist(), e));
		assertTrue(testProfile.addTitelOnIndexToPlayList(e,testProfile.getUserPlaylist(),0));
		assertTrue(testProfile.clearPlayList());
	}
	@Test
	public void testGetIndexOfList(){
		MuFi e = new MuFi("title","artist","album","genre",0);
		testProfile.addTitelEndToPlayList(e);
	}
	
	@Test
	public void testSetUsername(){
		testProfile.setUserName("testName");
		assertEquals("testName",testProfile.getUserName());
	}
	@Test 
	public void testSetUserMail(){
		testProfile.setUserEmail("testEmail@test.com");
		assertEquals(testProfile.getUserEmail(),"testEmail@test.com");
	}
	@Test
	public void testSetUserCredit(){
		testProfile.setUserCredit(10);
		assertEquals(10,testProfile.getUserCredit(),0);
	}
	@Test
	public void testSetUserStatus(){
		testProfile.setUserStatus(true);
		assertEquals(true,testProfile.getUserStatus());
	}
	@Test
	public void testCheckPassword(){
		assertTrue(testProfile.checkPassword("password"));
	}
	@Test
	public void testChangePassword() throws NotEqualException {
			testProfile.changePassword("newPassword");
			assertTrue(testProfile.checkPassword("newPassword"));
	}
	/**
	 *  Try und catch f�rbeabsichtigte Exceptionwurf kontrolle
	 * @throws NotEqualException
	 */
	
	@Test // (expected = NotEqualException.class)
	public void testChangeUserEmail() throws NotEqualException{
		final String testEmail = "testmai@aol.com";
		try{
			testProfile.changeUserEmail("isNotTheEmail@test.de",testEmail);
			fail("Expected exception!");
		}
		catch (NotEqualException e) {}
		testProfile.changeUserEmail(testEmail,testEmail);
		assertEquals(testProfile.getUserEmail(),testEmail);
	}
	/**
	 * müsste eigentlich in die Datenbank Test Klasse
	 */
	@Test
	public void testUpdateProfile(){
		database.insertProfile(testProfile);
		testProfile.setUserEmail("helpdesk@aol.com");
		testProfile.updateProfile();
		assertTrue(database.userNameExist(testProfile.getUserName()));
		assertEquals("helpdesk@aol.com",database.getProfile(testProfile.getUserName()).getUserEmail());
	}
	@Test
	public void testGetIndexOfElement(){
		MuFi e = new MuFi("title","artist","album","genre",0);
		testProfile.addTitelEndToTitleList(e);
		int index = testProfile.getIndexOfList(e , testProfile.getUserTitleList());
		assertEquals(testProfile.getIndexOfList(e,testProfile.getUserTitleList()),index);
	}
	@Test
	public void testDownloadMuFi() throws NotFoundException{
		MuFi e = new MuFi("title","artist","album","genre",0);
		testProfile.addTitelEndToTitleList(e);
			assertEquals("/ILoveMusic/artist/album/title.mp3",testProfile.downloadMuFi(e));
	}
	@Test
	public void testDownloadMusicFileList(){
		MuFi e = new MuFi("title","artist","album","genre",0);
		MuFi e1 = new MuFi("title1","artist1","album1","genre1",0);
		MuFi e2 = new MuFi("title2","artist2","album2","genre2",0);
		ArrayList<MuFi> elements = new ArrayList<MuFi>();
		elements.add(e);
		elements.add(e1);
		elements.add(e2);
		testProfile.addTitelEndToTitleList(e);
		testProfile.addTitelEndToTitleList(e1);
		testProfile.addTitelEndToTitleList(e2);
		List<String> pathList = testProfile.downloadMusicFileList();
		assertEquals(pathList.get(0), e.getPath());
		assertEquals(pathList.get(1), e1.getPath());
		assertEquals(pathList.get(2), e2.getPath());
	}
	@Test
	public void testdownloadPlaylist(){
		MuFi e = new MuFi("title","artist","album","genre",0);
		MuFi e1 = new MuFi("title1","artist1","album1","genre1",0);
		MuFi e2 = new MuFi("title2","artist2","album2","genre2",0);
		ArrayList<MuFi> elements = new ArrayList<MuFi>();
		elements.add(e);
		elements.add(e1);
		elements.add(e2);
		testProfile.addTitelEndToPlayList(e);
		testProfile.addTitelEndToPlayList(e1);
		testProfile.addTitelEndToPlayList(e2);
		List<String> pathList = testProfile.downloadPlaylist();
		assertEquals(pathList.get(0), e.getPath());
		assertEquals(pathList.get(1), e1.getPath());
		assertEquals(pathList.get(2), e2.getPath());
	}

}

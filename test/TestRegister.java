
import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import base.Database;
import base.Profile;
import base.Register;

public class TestRegister {
	
	private Database db = new Database();
	private Profile p;
	
	@Before
	public void setUp(){
		Register.createProfile("testUser2", "pw", "test@mail.com");
		p = db.getProfile("testUser2");
	}

	@After
	public void cleanup() {
		db.deleteProfile(p);
	}
	
	//Registrierung erfolgreich	
	@Test
	public void testCreateProfile1() {
		Register.createProfile("testUser2", "pw", "test@mail.com");
		assertEquals(db.getProfile("testUser2").getUserName(), "testUser2");
		assertEquals(db.getProfile("testUser2").getUserEmail(), "test@mail.com");
	}
}

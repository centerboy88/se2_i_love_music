import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	TestCoupon.class, 
	TestCredit.class,
	TestEmail.class,
	TestMuFi.class,
	TestPasswort.class,
	TestProfile.class,
	TestRegister.class,
	TestShoppingCart.class})
public class TestSuite {}

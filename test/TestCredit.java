import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import base.Credit;
import base.exceptions.NegativeNumberException;



/**
 * @author Dennis, Rene, Benjamin, Fritz, Michael
 *
 */
public class TestCredit {

	private Credit cr;
	
	@Before 
	public void newObject(){
		this.cr = new Credit();
	}
	
	
	@Test
	public void testGetCredit(){
		assertEquals(this.cr.getCredit(), 0,0);	
	}
	/**
	 * Ueberprueft die Richtigkeit der Addierung
	 * @throws NegativeNumberException wenn negatives Guthaben addiert wird
	 */
		
	@Test
	public void testAddCredit() throws NegativeNumberException{
		this.cr.addCredits(5);
		assertEquals(this.cr.getCredit(),5,0);
		try{
			this.cr.addCredits(-6);
		}
		catch (NegativeNumberException e){
			
		}
		
	/**
	 * Überprüftt die Richtigkeit der Subtrahierung, verhindert negative eingaben oder das das Guthaben unter Null sinkt
	 */	
	}	
	@Test
	public void testSubCredit() throws NegativeNumberException{
		
		this.cr.addCredits(6);
		this.cr.subCredits(5);
		assertEquals(this.cr.getCredit(),1,0); // True
		try{
			this.cr.subCredits(-1);
		}
		catch (NegativeNumberException e){
		}
	}
}
	
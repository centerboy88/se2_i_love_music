
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import base.Database;
import base.MuFi;
import base.Profile;
import base.ShoppingCart;

public class TestShoppingCart {

	private static Database db = new Database();
	private ShoppingCart cart = new ShoppingCart();
	private Profile p;
	private MuFi m1;
	private MuFi m2;
	
	@Before
	public void setUp(){
	 	p = new Profile("testUser", "pw", "test@mail.com");
		m1 = new MuFi(13, "Burn the Evidence", "Celine Dion", "Titanic", "Crap", 1.0f);
		m2 = new MuFi(14, "Smells like Teen Spirit", "Nirvana", "Nevermind", "Awesome", 5.0f);
	}
	
	@After
	public void cleanUp(){
		db.deleteMuFi(m1);
		db.deleteMuFi(m2);
		db.deleteProfile(p);
	}

	@Test
	public void testSearch() {
		for (MuFi temp:ShoppingCart.search("Burn the Evidence")) {
			assertEquals(temp.getId(), 13);
		}
	}
	

	@Test
	public void testAddTitleToCart() {
		assertTrue(cart.showcart().add(m1));
		assertTrue(cart.showcart().add(m2));
		assertEquals(cart.getSize(),2);
	}

	@Test
	public void testDeleteTitleFromCart() {
		assertTrue(cart.showcart().add(m1));
		assertTrue(cart.showcart().add(m2));
		cart.deleteTitleFromCart(13);
		assertEquals(cart.getSize(), 1);
	}

	@Test
	public void testEmptyCart() {
		assertTrue(cart.showcart().add(m1));
		assertTrue(cart.showcart().add(m2));
		cart.emptyCart();
		assertEquals(cart.getSize(), 0);
	}

	//Bezahlen erfolgreich, genug Credits
	//Testet außerdem ShoppingCart.getTotalPrice()
	@Test
	public void testPayCart1() {
		p.setUserCredit(10.0f);
		assertTrue(cart.showcart().add(m1));
		assertTrue(cart.showcart().add(m2));
		assertTrue(cart.payCart(p));
		assertEquals(p.getUserCredit(), 4.0f, 0);
		assertEquals(cart.getSize(), 0);
	}
	
	//Bezahlen fehlgeschlagen, zu wenig Credits
	@Test
	public void testPayCart2() {
		p.setUserCredit(5.0f);
		//cart.addTitleToCart(13);
		//cart.addTitleToCart(14);
		assertTrue(cart.showcart().add(m1));
		assertTrue(cart.showcart().add(m2));
		assertFalse(cart.payCart(p));
		assertEquals(p.getUserCredit(), 5.0f, 0);
		assertEquals(cart.getSize(), 2);
	}
}

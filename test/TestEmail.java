

import static org.junit.Assert.*;

import org.junit.Test;
import base.Profile;
import base.Email;

public class TestEmail {

	private final Profile profile = new Profile("testUser","testUserEmail", "pw");
	private final Email email = new Email(profile);
	

	@Test
	public void testSetBetreff() {
		email.setBetreff("TestBetreff");
		assertEquals(email.getBetreff(),"TestBetreff");
	}

	@Test
	public void testSetText() {
		email.setText("TestText");
		assertEquals(email.getText(), "TestText");	}

	@Test
	public void testSendMail() {
		//fail("FUNKTION Not yet implemented");
	}

	@Test
	public void testGetReciever() {
		assertEquals(email.getReciever(), "testUserEmail");
	}

	@Test
	public void testGetSender() {
		assertEquals(email.getSender(), "I_LOVE_MUSIC@fantastic.geek");
	}

	@Test
	public void testSetSender() {
		email.setSender("TestMail");
		assertEquals(email.getSender(),  "TestMail");
	}

}

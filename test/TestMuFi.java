

import static org.junit.Assert.*;

import org.junit.Test;

import base.MuFi;

public class TestMuFi {

	private MuFi m = new MuFi(13, "My Heart will go on", "Celine Dion", "Titanic", "Crap", 1.0f);

	@Test
	public void testGetId() {
		assertEquals(m.getId(), 13);
	}

	@Test
	public void testGetTitle() {
		assertEquals(m.getTitle(), "My Heart will go on");
	}

	@Test
	public void testGetArtist() {
		assertEquals(m.getArtist(), "Celine Dion");
	}

	@Test
	public void testGetAlbum() {
		assertEquals(m.getAlbum(), "Titanic");
	}

	@Test
	public void testGetGenre() {
		assertEquals(m.getGenre(), "Crap");
	}

	@Test
	public void testGetPrice() {
		assertEquals(m.getPrice(), 1.0f, 0);
	}

	@Test
	public void testGetPath() {
		assertEquals(m.getPath(), "/ILoveMusic/Celine Dion/Titanic/My Heart will go on.mp3");
	}

	@Test
	public void testSetTitle() {
		m.setTitle("testTitle");
		assertEquals(m.getTitle(), "testTitle");
	}

	@Test
	public void testSetArtist() {
		m.setArtist("testArtist");
		assertEquals(m.getArtist(), "testArtist");
	}

	@Test
	public void testSetAlbum() {
		m.setAlbum("testAlbum");
		assertEquals(m.getAlbum(), "testAlbum");
	}

	@Test
	public void testSetGenre() {
		m.setGenre("testCrap");
		assertEquals(m.getGenre(), "testCrap");
	}

	@Test
	public void testSetPrice() {
		m.setPrice(0.0f);
		assertEquals(m.getPrice(), 0.0f, 0);
	}
}

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import base.Database;
import base.Profile;
import  base.exceptions.NotEqualException;



/**
 * @author Dennis, Rene, Benjamin, Fritz, Michael
 * 
 */
public class TestPasswort {
	
	Profile p;
	Database db = new Database();

	@Before
	public  void setUp(){
			p = new Profile("userName","Email","password");
	}
	
	@Test 
	public void testCheckPassword(){
		assertTrue(p.checkPassword("password")); //true
		assertFalse(p.checkPassword("notuserPW")); //true
	}	
	@Test
	public void testChangePassword() throws NotEqualException{
		p.changePassword("newPassword");
		assertEquals("newPassword",p.getPassword());
	}
	
	@After
	public void cleanUp(){
		db.deleteProfile(p);
	}
}

	
		

